import { AuthService } from './../services/auth-services';
import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable()
export class UsernameValidator {

    debouncer: any;

    constructor(private auth: AuthService) { }

    checkUsername(control: FormControl): any {
        clearTimeout(this.debouncer);

        return new Promise(resolve => {
            this.debouncer = setTimeout(() => {
                this.auth.Validate(control.value, 'username').subscribe((res) => {
                    if (res.json()) {
                        resolve(null);
                    } else {
                        resolve({ 'usernameInUse': true });
                    }
                }, (err) => {
                    resolve({ 'usernameInUse': true });
                });
            }, 1000);
        });
    }
}
