import { AbstractControl } from '@angular/forms';
// string IdExpression = @"(?<Year>[0-9][0-9])(?<Month>([0][1-9])|([1][0-2]))"+
// "(?<Day>([0-2][0-9])|([3][0-1]))(?<Gender>[0-9])(?<Series>[0-9]{3})(?<Citizenship>[0-9])(?<Uniform>[0-9])(?<Control>[0-9])";
export class IdNumberValidation {

    static ValidateID(AC: AbstractControl) {
        const id = AC.get('idpass').value;
        const isValid = this.Validate(id);
        if (!isValid) {
            AC.get('idpass').setErrors({ idValid: true });
        } else {
            return null;
        }
    }

    static Validate(idNumber) {
        let correct = true;

        if (idNumber.length !== 13 || !this.isNumber(idNumber)) {
            correct = false;
        }

        // get first 6 digits as a valid date
        const tempDate = new Date(idNumber.substring(0, 2), idNumber.substring(2, 4) - 1, idNumber.substring(4, 6));

        const id_date = tempDate.getDate();
        const id_month = tempDate.getMonth();
        const id_year = tempDate.getFullYear();

        const fullDate = id_date + '-' + (id_month + 1) + '-' + id_year;
        if (!((tempDate.getFullYear() === idNumber.substring(0, 2)) &&
            (id_month === idNumber.substring(2, 4) - 1) &&
            (id_date === idNumber.substring(4, 6)))) {
            correct = false;
        }

        // get the gender
        const genderCode = idNumber.substring(6, 10);
        const gender = parseInt(genderCode, 0) < 5000 ? 'Female' : 'Male';

        // get country ID for citzenship
        const citzenship = parseInt(idNumber.substring(10, 11), 0) === 0 ? 'Yes' : 'No';

        // apply Luhn formula for check-digits
        let tempTotal = 0;
        let checkSum = 0;
        let multiplier = 1;
        for (let i = 0; i < 13; ++i) {
            tempTotal = parseInt(idNumber.charAt(i), 0) * multiplier;
            if (tempTotal > 9) {
                tempTotal = parseInt(tempTotal.toString().charAt(0), 0) + parseInt(tempTotal.toString().charAt(1), 0);
            }
            checkSum = checkSum + tempTotal;
            multiplier = (multiplier % 2 === 0) ? 1 : 2;
        }
        if ((checkSum % 10) !== 0) {
            correct = false;
        }

        // if no error found, hide the error message
        if (correct) {
            return true;
        }
        return false;
    }

    static isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
}
