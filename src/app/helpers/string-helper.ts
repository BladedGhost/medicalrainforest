export class StringHelper {
    public static ToBase64(value: string): string {
        return btoa(value);
    }

    public static FromBase64(value: string): string {
        return atob(value);
    }
}
