export class StorageConstants {
    public static LogonKey = 'UserToken';
    public static UserType = 'UserType';
    public static UserSettings = 'UserSettings';
    public static LoggedIn = 'loggedin';
    public static StoreData = 'StoreData';
    public static RememberMe = 'rememberMe';
    public static SignUp = 'SignUp';
    public static DoctorSchedule = 'DoctorSchedule';
    public static Menu = [];
}

export enum Days {
    Monday = 0,
    Tuesday = 1,
    Wednesday = 2,
    Thursday = 3,
    Friday = 4,
    Saturday = 5,
    Sunday = 6
}
