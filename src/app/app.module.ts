import { SchedulersService } from './services/schedular-service';
import { SchedulerModule } from './components/scheduler/scheduler.module';
import { MedicalAidService } from './services/medical-aid';
import { VerifyInvoiceModule } from './components/patient/invoices/verify-invoice/verify-invoice.module';
import { AboutUsModule } from './components/about-us/about-us.module';
import { MailService } from './services/mail.service';
import { NewPasswordModule } from './components/sign-in/new-password/new-password.module';
import { ReceptionManageModule } from './components/doctor/reception/reception-manage/reception-manage.module';
import { NotificationService } from './services/notification.service';
import { ManageInvoicesModule } from './components/doctor/invoices/manage-invoices/manage-invoices.module';
import { DoctorInvoicesModule } from './components/doctor/invoices/invoices.module';
import { PatientDataModule } from './components/doctor/patient-data/patient-data.module';
import { PatientFilesModule } from './components/doctor/patient-files/patient-files.module';
import { DoctorService } from './services/doctor.service';
import { PatientShareModule } from './components/doctor/bank-details/patient-share/patient-share.module';
import { ManageBankDetailsModule } from './components/doctor/bank-details/manage-bank-details/manage-bank-details.module';
import { ProfileModule } from './components/profile/profile.module';
import { PatientBankDetailsModule } from './components/patient/bank-details/bank-details.module';
import { BankDetailsModule } from './components/doctor/bank-details/bank-details.module';
import { DashboardModule } from './components/dashboard/dashboard.module';
import { PaymentsModule } from './components/patient/payments/payments.module';
import { BankDetailsService } from './services/bank-details.service';
import { FileManagerModule } from './components/documents/file-manager/file-manager.module';
import { DocumentService } from './services/document.service';
import { PatientService } from './services/patient.service';
import { BarcodeModule } from './components/doctor/barcode/barcode.module';
import { LookupService } from './services/lookup-service';
import { GlobalErrorHandlerService } from './services/global-error-handler.service';
import { HttpWrapperService } from './services/http-wrapper.service';
import { DocumentsModule } from './components/documents/documents.module';
import { MessageModule } from './components/mail/message/message.module';
import { ArchiveModule } from './components/mail/archive/archive.module';
import { MailModule } from './components/mail/mail.module';
import { OutstandingInvoicesModule } from './components/patient/invoices/outstanding-invoices/outstanding-invoices.module';
import { MessageService } from 'primeng/components/common/messageservice';
import { SessionService } from './services/session-service';
import { SignInModule } from './components/sign-in/sign-in.module';
import { AuthService } from './services/auth-services';
/*Main App Stuff*/
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import { HttpModule, XHRBackend, RequestOptions, Http } from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {AppRoutes} from './app.routes';
import 'rxjs/add/operator/toPromise';
/*Modules*/
import { AccordionModule, ConfirmationService } from 'primeng/primeng';
import {AutoCompleteModule} from 'primeng/primeng';
import {BreadcrumbModule} from 'primeng/primeng';
import {ButtonModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';
import {CarouselModule} from 'primeng/primeng';
import {ChartModule} from 'primeng/primeng';
import {CheckboxModule} from 'primeng/primeng';
import {ChipsModule} from 'primeng/primeng';
import {CodeHighlighterModule} from 'primeng/primeng';
import {ConfirmDialogModule} from 'primeng/primeng';
import {ColorPickerModule} from 'primeng/primeng';
import {SharedModule} from 'primeng/primeng';
import {ContextMenuModule} from 'primeng/primeng';
import {DataGridModule} from 'primeng/primeng';
import {DataListModule} from 'primeng/primeng';
import {DataScrollerModule} from 'primeng/primeng';
import {DataTableModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';
import {DragDropModule} from 'primeng/primeng';
import {DropdownModule} from 'primeng/primeng';
import {EditorModule} from 'primeng/primeng';
import {FieldsetModule} from 'primeng/primeng';
import {FileUploadModule} from 'primeng/primeng';
import {GalleriaModule} from 'primeng/primeng';
import {GMapModule} from 'primeng/primeng';
import {GrowlModule} from 'primeng/primeng';
import {InputMaskModule} from 'primeng/primeng';
import {InputSwitchModule} from 'primeng/primeng';
import {InputTextModule} from 'primeng/primeng';
import {InputTextareaModule} from 'primeng/primeng';
import {LightboxModule} from 'primeng/primeng';
import {ListboxModule} from 'primeng/primeng';
import {MegaMenuModule} from 'primeng/primeng';
import {MenuModule} from 'primeng/primeng';
import {MenubarModule} from 'primeng/primeng';
import {MessagesModule} from 'primeng/primeng';
import {MultiSelectModule} from 'primeng/primeng';
import {OrderListModule} from 'primeng/primeng';
import {OrganizationChartModule} from 'primeng/primeng';
import {OverlayPanelModule} from 'primeng/primeng';
import {PaginatorModule} from 'primeng/primeng';
import {PanelModule} from 'primeng/primeng';
import {PanelMenuModule} from 'primeng/primeng';
import {PasswordModule} from 'primeng/primeng';
import {PickListModule} from 'primeng/primeng';
import {ProgressBarModule} from 'primeng/primeng';
import {RadioButtonModule} from 'primeng/primeng';
import {RatingModule} from 'primeng/primeng';
import {ScheduleModule} from 'primeng/primeng';
import {SelectButtonModule} from 'primeng/primeng';
import {SlideMenuModule} from 'primeng/primeng';
import {SliderModule} from 'primeng/primeng';
import {SpinnerModule} from 'primeng/primeng';
import {SplitButtonModule} from 'primeng/primeng';
import {StepsModule} from 'primeng/primeng';
import {TabMenuModule} from 'primeng/primeng';
import {TabViewModule} from 'primeng/primeng';
import {TerminalModule} from 'primeng/primeng';
import {TieredMenuModule} from 'primeng/primeng';
import {ToggleButtonModule} from 'primeng/primeng';
import {ToolbarModule} from 'primeng/primeng';
import {TooltipModule} from 'primeng/primeng';
import {TreeModule} from 'primeng/primeng';
import {TreeTableModule} from 'primeng/primeng';
import { RegistrationModule } from './components/sign-in/registration/registration.module';
import { LandingModule } from './components/landing/landing.module';
/*Components*/
import {AppComponent} from './components/app-component/app.component';
import {AppMenuComponent, AppSubMenuComponent} from './components/app-menu/app.menu.component';
import {AppBreadcrumbComponent} from './components/app-breadcrumb/app.breadcrumb.component';
import {AppTopBarComponent} from './components/app-topbar/app.topbar.component';
import {AppFooterComponent} from './components/app-footer/app.footer.component';
/*Services*/
import {BreadcrumbService} from './services/breadcrumb.service';
import { AlwaysOnGuard, OnlyLoggedInUsersGuard, ChildOnGuard, OnlyLoggedOutUsersGuard } from './services/route-guard.service';
import { AccessDeniedComponent } from './components/access-denied/access-denied.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ErrorComponent } from './components/error/error.component';
import { FamilyProfileComponent } from './components/patient/family-profile/family-profile.component';
import { PaymentsComponent } from './components/patient/payments/payments.component';
import { PatientInvoicesComponent } from './components/patient/invoices/invoices.component';
import { DocumentsComponent } from './components/documents/documents.component';
import { BankDetailsComponent } from './components/doctor/bank-details/bank-details.component';
import { PatientFilesComponent } from './components/doctor/patient-files/patient-files.component';
import { MailComponent } from './components/mail/mail.component';
import { DoctorInvoicesComponent } from './components/doctor/invoices/invoices.component';
import { GlobalMessagesService } from './services/global-messages.service';
import { FamilyProfileModule } from './components/patient/family-profile/family-profile.module';
import { ManageModule } from './components/patient/family-profile/manage/manage.module';
import { TAndCModule } from './components/t-and-c/t-and-c.module';
import { NewInvoicesModule } from './components/patient/invoices/new-invoices/new-invoices.module';
import { PaidInvoicesModule } from './components/patient/invoices/paid-invoices/paid-invoices.module';
import { InboxModule } from './components/mail/inbox/inbox.module';
import { httpFactory } from './interceptors';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UsernameValidator } from './helpers/UsernameValidator';
import { ReceptionModule } from './components/doctor/reception/reception.module';
import { InvoiceEmailModule } from './components/doctor/invoices/email/email.module';
import { UploadConditionsModule } from './components/profile/upload-conditions/upload-conditions.module';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutes,
        HttpModule,
        BrowserAnimationsModule,
        AccordionModule,
        AutoCompleteModule,
        BreadcrumbModule,
        ButtonModule,
        CalendarModule,
        CarouselModule,
        ChartModule,
        CheckboxModule,
        ChipsModule,
        CodeHighlighterModule,
        ConfirmDialogModule,
        ColorPickerModule,
        SharedModule,
        ContextMenuModule,
        DataGridModule,
        DataListModule,
        DataScrollerModule,
        DataTableModule,
        DialogModule,
        DragDropModule,
        DropdownModule,
        EditorModule,
        FieldsetModule,
        FileUploadModule,
        GalleriaModule,
        GMapModule,
        GrowlModule,
        InputMaskModule,
        InputSwitchModule,
        InputTextModule,
        InputTextareaModule,
        LightboxModule,
        ListboxModule,
        MegaMenuModule,
        MenuModule,
        MenubarModule,
        MessagesModule,
        MultiSelectModule,
        OrderListModule,
        OrganizationChartModule,
        OverlayPanelModule,
        PaginatorModule,
        PanelModule,
        PanelMenuModule,
        PasswordModule,
        PickListModule,
        ProgressBarModule,
        RadioButtonModule,
        RatingModule,
        ScheduleModule,
        SelectButtonModule,
        SlideMenuModule,
        SliderModule,
        SpinnerModule,
        SplitButtonModule,
        StepsModule,
        TabMenuModule,
        TabViewModule,
        TerminalModule,
        TieredMenuModule,
        ToggleButtonModule,
        ToolbarModule,
        TooltipModule,
        TreeModule,
        TreeTableModule,
        RegistrationModule,
        LandingModule,
        SignInModule,
        FamilyProfileModule,
        ManageModule,
        TAndCModule,
        NewInvoicesModule,
        PaidInvoicesModule,
        OutstandingInvoicesModule,
        MailModule,
        InboxModule,
        ArchiveModule,
        MessageModule,
        DocumentsModule,
        NgxQRCodeModule,
        BarcodeModule,
        FileManagerModule,
        PaymentsModule,
        DashboardModule,
        BankDetailsModule,
        PatientBankDetailsModule,
        ManageBankDetailsModule,
        ProfileModule,
        PatientShareModule,
        PatientFilesModule,
        PatientDataModule,
        DoctorInvoicesModule,
        ManageInvoicesModule,
        ReceptionModule,
        ReceptionManageModule,
        NewPasswordModule,
        UploadConditionsModule,
        AboutUsModule,
        VerifyInvoiceModule,
        SchedulerModule
    ],
    declarations: [
        AppComponent,
        AppMenuComponent,
        AppSubMenuComponent,
        AppBreadcrumbComponent,
        AppTopBarComponent,
        AppFooterComponent,
        AccessDeniedComponent,
        NotFoundComponent,
        ErrorComponent,
        PatientInvoicesComponent,
        GlobalMessagesService
    ],
    providers: [
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        BreadcrumbService,
        AlwaysOnGuard, OnlyLoggedInUsersGuard, OnlyLoggedOutUsersGuard, ChildOnGuard, AuthService,
        SessionService, MessageService, GlobalMessagesService, ConfirmationService,
        HttpWrapperService, NotificationService, InvoiceEmailModule,
        {
            provide: Http,
            useFactory: httpFactory,
            deps: [XHRBackend, RequestOptions]
        }, LookupService, GlobalErrorHandlerService, PatientService, DocumentService, SchedulersService,
        BankDetailsService, UsernameValidator, DoctorService, MailService, MedicalAidService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
