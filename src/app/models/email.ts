export class Email {
    public from: string;
    public to: string;
    public subject: string;
    public body: string;
    public cc: string;
    public bcc: string;
    public attachement: Array<any>;
}
