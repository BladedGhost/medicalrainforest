export class LogonDetails {
    token: string;
    loginSuccess: string;
    loginMessage: string;
    tokenExpires: Date;
    userData: any;
}
