export class ProfileModel {
    userId: number;
    name: string;
    surname: string;
    email: string;
    phone: string;
    username: string;
    password: string;
    confirm: string;
    doctorId: number;
    idNumber: string;
    hpcsa: string;
    practiceNumber: string;
    practicePhone: string;
    diciplineId: number;
    dicipline: string;
    role: string;
    status: string;
    loggedIn: string;
    lockedOut: string;
    updatedBy: string;
    success: boolean;
    menu: any;
}
