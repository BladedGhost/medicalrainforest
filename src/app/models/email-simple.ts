export class EmailSimple {
    public emailId: number;
    public from: string;
    public to: string;
    public subject: string;
    public body: string;
    public cc: string;
    public bcc: string;
    public storeType: string;
    public updatedBy: string;
    public attachement: any;
    public AttachementFiles: string;
}
