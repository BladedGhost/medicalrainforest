import { SchedulerComponent } from './components/scheduler/scheduler.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { ReceptionManageComponent } from './components/doctor/reception/reception-manage/reception-manage.component';
import { ManageInvoicesComponent } from './components/doctor/invoices/manage-invoices/manage-invoices.component';
import { PatientDataComponent } from './components/doctor/patient-data/patient-data.component';
import { ManageBankDetailsComponent } from './components/doctor/bank-details/manage-bank-details/manage-bank-details.component';
import { PatientBankDetailsComponent } from './components/patient/bank-details/bank-details.component';
import { FileManagerComponent } from './components/documents/file-manager/file-manager.component';
import { NewInvoicesComponent } from './components/patient/invoices/new-invoices/new-invoices.component';
import { ManageComponent } from './components/patient/family-profile/manage/manage.component';
import { PatientFilesComponent } from './components/doctor/patient-files/patient-files.component';
import { RegistrationComponent } from './components/sign-in/registration/registration.component';
import { PatientInvoicesComponent } from './components/patient/invoices/invoices.component';
import { ErrorComponent } from './components/error/error.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { AlwaysOnGuard, OnlyLoggedInUsersGuard, OnlyLoggedOutUsersGuard } from './services/route-guard.service';
import { LandingComponent } from './components/landing/landing.component';
import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AccessDeniedComponent } from './components/access-denied/access-denied.component';
import { MailComponent } from './components/mail/mail.component';
import { FamilyProfileComponent } from './components/patient/family-profile/family-profile.component';
import { DocumentsComponent } from './components/documents/documents.component';
import { PaymentsComponent } from './components/patient/payments/payments.component';
import { BankDetailsComponent } from './components/doctor/bank-details/bank-details.component';
import { DoctorInvoicesComponent } from './components/doctor/invoices/invoices.component';
import { TAndCComponent } from './components/t-and-c/t-and-c.component';
import { PaidInvoicesComponent } from './components/patient/invoices/paid-invoices/paid-invoices.component';
import { OutstandingInvoicesComponent } from './components/patient/invoices/outstanding-invoices/outstanding-invoices.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ReceptionComponent } from './components/doctor/reception/reception.component';

export const routes: Routes = [
    {path: '', component: LandingComponent, canActivate: [OnlyLoggedOutUsersGuard]},
    {path: 'signin', component: SignInComponent, canActivate: [OnlyLoggedOutUsersGuard]},
    {path: 'signup', component: SignInComponent, canActivate: [OnlyLoggedOutUsersGuard]},
    {path: 'About-Us', component: AboutUsComponent, canActivate: [OnlyLoggedOutUsersGuard]},
    {path: 'signin/:token', component: SignInComponent, canActivate: [OnlyLoggedOutUsersGuard]},
    {path: 'signin/:token/:verify', component: SignInComponent, canActivate: [OnlyLoggedOutUsersGuard]},
    {path: 'register', component: RegistrationComponent},
    {path: 'TnC', component: TAndCComponent},
    {path: 'dashboard', component: DashboardComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'mail', component: MailComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'bookings', component: SchedulerComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'bookings/:date', component: SchedulerComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'bookings/:date/:docId/:id', component: SchedulerComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'bookings/:date/:id', component: SchedulerComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'family-profile', component: FamilyProfileComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'family-manage', component: ManageComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'documents', component: DocumentsComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'patient-bank-details', component: PatientBankDetailsComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'profile', component: ProfileComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'payments', component: PaymentsComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'bank-details', component: BankDetailsComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'bank-details/manage', component: ManageBankDetailsComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'patient-invoices', canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard],
        children: [
            {path: '', redirectTo: 'new-invoices', pathMatch: 'full'},
            {path: 'new-invoices', component: NewInvoicesComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
            {path: 'new-invoices/:id', component: NewInvoicesComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
            {path: 'paid-invoices', component: PaidInvoicesComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
            {path: 'outstanding-invoices', component: OutstandingInvoicesComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
            {path: '**', redirectTo: 'new-invoices', pathMatch: 'prefix'}
        ]},
    {path: 'doctor-invoices', component: DoctorInvoicesComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'doctor-invoices/manage', component: ManageInvoicesComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'patient-files', component: PatientFilesComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'patient-files/patient', component: PatientDataComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'patient-files/patient/:token', component: PatientDataComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'file-manager', component: FileManagerComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'reception', component: ReceptionComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'reception/manage', component: ReceptionManageComponent, canActivate: [AlwaysOnGuard, OnlyLoggedInUsersGuard]},
    {path: 'denied', component: AccessDeniedComponent},
    {path: 'error', component: ErrorComponent},
    {path: '**', component: NotFoundComponent}
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
