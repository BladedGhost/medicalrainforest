import {Component} from '@angular/core';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';

@Component({
    templateUrl: './global-messsage.html'
})

// tslint:disable-next-line:component-class-suffix
export class GlobalMessagesService {
    msgs = [];
    constructor(private messageService: MessageService) {}

    addSingle(message: Message) {
        this.messageService.add(message);
    }

    addMultiple(message: Message[]) {
        this.messageService.addAll(message);
    }

    clear() {
        this.messageService.clear();
    }
}
