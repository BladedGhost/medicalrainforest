import { Observable } from 'rxjs/Observable';
import { SessionService } from './session-service';
import { Injectable } from '@angular/core';
import { HttpWrapperService } from './http-wrapper.service';

@Injectable()
export class DocumentService {

  constructor(private store: SessionService, private _http: HttpWrapperService) { }

  GetAll(userId): Observable<any> {
    return this._http.get(`documents/get/all/${userId}`, true);
  }

  GetFile(fileId): Observable<any> {
    return  this._http.get(`get/file/${fileId}`, true);
  }

  GetDoctorInvoices(doctorId, type): Observable<any> {
    return  this._http.get(`documents/get/doctor/${doctorId}/${type}`, true);
  }

  GetpatientInvoices(userId, type): Observable<any> {
    return  this._http.get(`documents/get/patient/${userId}/${type}`, true);
  }

  CheckInvoiceVerified(invoiceId: number) {
    return this._http.get(`documents/CheckInvoiceVerified/${invoiceId}`, true);
  }

  UpdateFile(file): Observable<any> {
    return  this._http.post('documents/', file, true);
  }

  UpdateInvoiceType(invoiceType, invoiceId): Observable<any> {
    const data = {invoiceType: invoiceType, invoiceId: invoiceId};
    return  this._http.post('documents/UpdateInvoiceType', data, true);
  }

  CreateFile(file): Observable<any> {
    return this._http.put('documents/', file, true);
  }

  DeleteFile(fileId): Observable<any> {
    return this._http.delete(`documents/${fileId}`, true);
  }

  DeleteInvoice(fileId): Observable<any> {
    return this._http.delete(`documents/invoice/${fileId}`, true);
  }

  DownloadFile(fileId) {
    window.open(`${this._http.UploadUrl}/download/${fileId}`);
  }

  DownloadInvoice(fileId) {
    window.open(`${this._http.UploadUrl}/download/invoice/${fileId}`);
  }

  UpdatePaid(invoiceId, paid) {
    return this._http.post(`documents/paid/${invoiceId}/${paid}`, null, true);
  }
}
