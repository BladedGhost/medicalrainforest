import { HttpWrapperService } from './http-wrapper.service';
import { Injectable } from '@angular/core';

@Injectable()
export class SchedulersService {

  constructor(private _http: HttpWrapperService) { }

  Create(data) {
    return this._http.put('Scheduler', data, true);
  }

  Update(data) {
    return this._http.post('Scheduler', data, true);
  }

  Get(doctorId) {
    return this._http.get(`Scheduler/${doctorId}/null`, true);
  }

  GetDoctors(userId) {
    return this._http.get(`Scheduler/doctors/${userId}`, true);
  }

  GetAll(userId) {
    return this._http.get(`Scheduler/all/${userId}`, true);
  }

  Delete(schedulerId) {
    return this._http.delete(`Scheduler/${schedulerId}`, true);
  }
}
