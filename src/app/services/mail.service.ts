import { Http } from '@angular/http';
import { HttpWrapperService } from './http-wrapper.service';
import { Injectable } from '@angular/core';
import { EmailSimple } from '../models/email-simple';

@Injectable()
export class MailService {

  constructor(private _http: HttpWrapperService, private http: Http) { }

  SendEmailPlain(mailOptions: any, useFormsMetod: boolean) {
    return this._http.post('email/send', mailOptions, true, (useFormsMetod ? 'multipart/form-data' : ''));
  }

  SendEmailPlainManual(mailOptions: any, useFormsMetod: boolean) {
    return this.http.post(this._http.EmailUrl + '/sendwithattach', mailOptions);
  }

  SendInvoiceItem(data: any) {
    return this._http.post(`email/emailInvoice`, data, true);
  }

  SendSupportEmail(mailOptions: EmailSimple) {
    return this._http.post('email/sendsupport', mailOptions, true);
  }

  SendInvoiceToMedicalaid (invoiceId: number) {
    return this._http.post(`email/emailInvoice/medicalaid/${invoiceId}`, null, true);
  }

  GetEmail(emailAddress: string, emailType: string) {
    return  this._http.get(`email/${emailAddress}/${emailType}`, true);
  }

  GetMainEmailAddress() {
    return  this._http.get(`email/getmain`, true);
  }

  GetPatientUIds(doctorId: number) {
    return this._http.get(`email/patientUIDs/${doctorId}`, true);
  }

  DeleteEmail(emailId: number) {
    return  this._http.delete(`email/${emailId}`, true);
  }

  ArchiveEmail(emailId: number) {
    return  this._http.post(`email/archive/${emailId}`, null, true);
  }

  GetEmailPeople(userId, userType) {
    return this._http.get(`email/peopele/${userId}/${userType}`, true);
  }
}
