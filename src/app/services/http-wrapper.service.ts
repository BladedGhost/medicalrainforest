import { environment } from './../../environments/environment';
import { GlobalErrorHandlerService } from './global-error-handler.service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams, ResponseContentType  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { MessageService } from 'primeng/components/common/messageservice';

@Injectable()
export class HttpWrapperService {
    // #region Properties
    private _headers: Headers;
    // private _url = 'http://localhost:60454/api/';
    // private _url = 'http://196.41.127.153:8010/api/';
    private _url = environment.url;
    public UploadUrl = this._url + 'documents';
    public EmailUrl = this._url + 'email';
    public TnCUrl = this._url + 'account/uploadTnC';
    public TnCDownloadUrl = this._url + 'account/download/tnc';
    // #endregion Properties

    // #region Constructor
    constructor(protected http: Http, protected _globalErrorHandler: GlobalErrorHandlerService, private _router: Router,
        private messageService: MessageService) { }
    // #endregion Constructor

    // #region Functions
    get(path: string, useDefaultHeaders: boolean, contentType?: string, params?: URLSearchParams): Observable<any> {
        path = this._url + path;
        if (useDefaultHeaders) {
            this.addDefaultHeaders(contentType);
        } else {
            this.addLogonHeaders();
        }
        console.log('GET', path);
        const options: RequestOptions = new RequestOptions({
            params: params,
            headers: this._headers
        });
        return this.http.get(path, options)
        .map(res => this.checkForError(res))
        .catch(err => this.manageException(err));
    }
    post(path: string, data: any, useDefaultHeaders: boolean, contentType?: string, params?: URLSearchParams,
        responseContentType?: ResponseContentType): Observable<any> {
        path = this._url + path;
        if (useDefaultHeaders) {
            this.addDefaultHeaders(contentType);
        } else {
            this.addLogonHeaders();
        }
        console.log('POST', path);
        const options: RequestOptions = new RequestOptions({
            params: params,
            headers: this._headers
        });

        if (responseContentType !== null) {
        options.responseType = responseContentType;
        }

        const body = !contentType || contentType === null ||
            contentType === '' || contentType === 'application/json'
            ? JSON.stringify(data) :
            data;
        // console.log('JSON:', body);
        return this.http.post(path, body, options)
        .map(res => this.checkForError(res))
        .catch(err => this.manageException(err));
    }

    delete(path: string, useDefaultHeaders: boolean, params?: URLSearchParams, contentType?: string): Observable<any> {
        path = this._url + path;
        if (useDefaultHeaders) {
            this.addDefaultHeaders(contentType);
        } else {
            this.addLogonHeaders();
        }
        const options: RequestOptions = new RequestOptions({
            params: params,
            headers: this._headers
        });
        console.log('DELETE', path);
        return this.http.delete(path, options)
        .map(res => {
            if (res.json() === true) {
                this.messageService.add({severity: 'success', summary: 'Delete', detail: 'Record has been deleted successfully.'});
            } else {
                this.messageService.add({severity: 'error', summary: 'Delete', detail: 'We could not delete this record, please try\
                     again or contact your local administrator.'});
            }
            return this.checkForError(res);
        })
        .catch(err => this.manageException(err));
    }
    deleteNoNotify(path: string, useDefaultHeaders: boolean, params?: URLSearchParams, contentType?: string): Observable<any> {
        path = this._url + path;
        if (useDefaultHeaders) {
            this.addDefaultHeaders(contentType);
        } else {
            this.addLogonHeaders();
        }
        const options: RequestOptions = new RequestOptions({
            params: params,
            headers: this._headers
        });
        console.log('DELETE', path);
        return this.http.delete(path, options)
        .map(res => {
            return this.checkForError(res);
        })
        .catch(err => this.manageException(err));
    }
    put(path: string, data: any, useDefaultHeaders: boolean, params?: URLSearchParams): Observable<any> {
        path = this._url + path;
        if (useDefaultHeaders) {
            this.addDefaultHeaders();
        } else {
            this.addLogonHeaders();
        }
        const options: RequestOptions = new RequestOptions({
            params: params,
            headers: this._headers
        });
        console.log('PUT', path);
        const body = JSON.stringify(data);
        return this.http.put(path, body, options)
        .map(res => this.checkForError(res))
        .catch(err => this.manageException(err));
    }
    private addLogonHeaders() {
        this._headers = new Headers();
        this._headers.append('Content-Type', 'application/x-www-form-urlencoded');
    }
    private addDefaultHeaders(contentType?: string) {
        if (!contentType || contentType === null || contentType === '') {
            contentType = 'application/json';
        }

        this._headers = new Headers();
        this._headers.append('Access-Control-Allow-Origin', '*');
        this._headers.append('Access-Control-Allow-Headers', 'Content-Type');
        if (contentType === 'application/json') {
        this._headers.append('Content-Type', contentType);
        this._headers.append('Accept', contentType);
        }
    }
    private checkForError(response: Response): Response {
        if (response.status >= 200 && response.status <= 300) {
            return response;
        }
        const error = new Error(response.statusText);
        error['response'] = response;
        // Todo: manage by the Global Error handler
        this._globalErrorHandler.customHandleError(error, false);
    }
    private manageException(error: Response) {
        console.log(error);
        // this.messageService.add({severity: 'error', summary: '', detail: 'An error has occured, please try again\
        //  or contact your local administrator'});
        if (error.status === 401) {
            this._router.navigateByUrl('/access-denied');
        }
        this._globalErrorHandler.customHandleError(error, false);
        return Observable.throw(error || 'Server error');
    }
    // #endregion Functions
}
