import { HttpWrapperService } from './http-wrapper.service';
import { Injectable } from '@angular/core';

@Injectable()
export class MedicalAidService {
    constructor(private _http: HttpWrapperService) {}
    GetMedicalAids() {
        return this._http.get('medicalAid', true);
    }
    GetMedicalAid(medicalaidId: number) {
        return this._http.get(`medicalAid/${medicalaidId}`, true);
    }
    GetMedicalaidByInvoice(invoiceId: number) {
        return this._http.get(`medicalAid/invoice/${invoiceId}`, true);
    }
}
