import { Observable } from 'rxjs/Rx';
import { HttpWrapperService } from './http-wrapper.service';
import { Injectable } from '@angular/core';
import { SelectItem } from 'primeng/components/common/selectitem';

@Injectable()
export class LookupService {

  constructor(private _http: HttpWrapperService) { }

  GetLookupsByType(type: string): Observable<any> {
    return this._http.get(`lookups/get/${type}`, true);
  }

  GetLookupValue(typeId: number): Observable<any> {
    return this._http.get(`lookups/getValue/${typeId}`, true);
  }

  GetLookupsFormated(type) {
    return this.GetLookupsByType(type).map(s => {
      const items = <Array<SelectItem>>s.json().map(x => {
        return <SelectItem>{
          label: x.value,
          value: x.lookupValueId
        };
      });
      return items;
    });
  }

  GetUploadUrl() {
    return this._http.UploadUrl;
  }

  GetTnCUploadUrl() {
    return this._http.TnCUrl;
  }

  GetTnCDownloadUrl(doctorId: number) {
    return this._http.TnCDownloadUrl + '/' + doctorId;
  }
}
