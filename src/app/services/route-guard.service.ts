import { AuthService, LoginType } from './auth-services';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { CanActivateChild } from '@angular/router/src/interfaces';

@Injectable()
export class AlwaysOnGuard implements CanActivate {
    constructor(private auth: AuthService, private route: Router) {}
    canActivate() {
        return true;
    }
}

@Injectable()
export class OnlyLoggedInUsersGuard implements CanActivate {
    constructor(private auth: AuthService, private route: Router) {}
    canActivate() {
        if (this.auth.isLoggedIn()) {
            return true;
        } else {
            this.route.navigateByUrl('denied');
            return false;
        }
    }
}
@Injectable()
export class OnlyLoggedOutUsersGuard implements CanActivate {
    constructor(private auth: AuthService, private route: Router) {}
    canActivate() {
        if (!this.auth.isLoggedIn()) {
            return true;
        } else {
            this.route.navigateByUrl('dashboard');
            return false;
        }
    }
}

@Injectable()
export class IsPateintGuard implements CanActivate {
    constructor(private auth: AuthService, private route: Router) {}
    canActivate() {
        if (this.auth.isLoggedIn() && this.auth.LoggedInAs() === LoginType.Patient) {
            return true;
        } else {
            this.route.navigateByUrl('denied');
            return false;
        }
    }
}

@Injectable()
export class IsDoctorGuard implements CanActivate {
    constructor(private auth: AuthService, private route: Router) {}
    canActivate() {
        console.log('Always On Guard');
        if (this.auth.isLoggedIn() && this.auth.LoggedInAs() === LoginType.Doctor) {
            return true;
        } else {
            window.alert('You don\'t have permission to view this page');
            this.route.navigateByUrl('denied');
            return false;
        }
    }
}

@Injectable()
export class ChildOnGuard implements CanActivateChild {
    constructor(private route: Router) {}
    canActivateChild() {
        console.log('Child Guarded');
        this.route.navigateByUrl('denied');
        return false;
    }
}
