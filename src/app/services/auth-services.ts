import { SchedulersService } from './schedular-service';
import { MessageService } from 'primeng/components/common/messageservice';
import { StorageConstants } from './../constants/storage-constants';
import { UserLogonModel } from './../models/user-login-model';
import { Observable } from 'rxjs/Observable';
import { HttpWrapperService } from './http-wrapper.service';
import { ProfileModel } from './../models/profile-model';
import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { SessionService } from './session-service';
import { Router } from '@angular/router';
import { AppComponent } from '../components/app-component/app.component';

@Injectable()
export class AuthService {
    private userLoggedIn = false;

    constructor(private store: SessionService, private route: Router, private location: Location,
        private _http: HttpWrapperService, private messageService: MessageService,
        private schedule: SchedulersService) { }

    isLoggedIn(): boolean {
        return this.store.GetValue(StorageConstants.LoggedIn) === 'true';
    }

    LoggedInAs(): string {
        return this.store.GetValue(StorageConstants.UserType);
    }

    Login(loginModel) {
        loginModel.appCall = 'Main';
        this._http.post('account/login', loginModel, true).subscribe(x => {
            const loginData = x.json();
            const userData = loginData.userData;
            if (loginData.loginSuccess) {
                sessionStorage.setItem(StorageConstants.LogonKey, JSON.stringify(loginData));
                if (userData.doctorId !== undefined && userData.doctorId !== null && userData.doctorId > 0) {
                    this.schedule.Get(userData.doctorId).subscribe(y => {
                        this.store.SetValue(StorageConstants.DoctorSchedule, JSON.stringify(y.json()));
                    });
                } else {
                    this.schedule.GetAll(userData.userId).subscribe(y => {
                        this.store.SetValue(StorageConstants.DoctorSchedule, JSON.stringify(y.json()));
                    });
                }
                this.messageService.add({severity: 'success', summary: 'Sign In', detail: 'User signed in successfully.'});
                this.store.SetValue(StorageConstants.UserType, userData.role, loginModel.rememberMe);
                StorageConstants.Menu = userData.menu = JSON.parse(JSON.stringify(userData.menu).replace(/,\"items\":\[\]/gi, ''));
                const user = {
                    username: userData.username,
                    type: this.store.GetValue(StorageConstants.UserType),
                    data: userData,
                    isDoctor: userData.doctorId !== undefined && userData.doctorId !== null && userData.doctorId > 0
                };
                this.store.SetUserData(user, loginModel.rememberMe);
                this.store.SetValue(StorageConstants.LoggedIn, 'true', loginModel.rememberMe);
                this.route.navigateByUrl('dashboard');
            } else {
                this.messageService.add({severity: 'error', summary: 'Sign In', detail: loginData.loginMessage});
            }
        });
    }

    Logout() {
        this.route.navigateByUrl('');
        this.location.replaceState('', '');
        this.store.ClearSession();
    }

    GetUserSettings() {
        return this.store.GetUserData();
    }

    Register(profile: ProfileModel): Observable<any> {
        return this._http.post('account/register', profile, true);
    }

    Update(profile: ProfileModel): Observable<any> {
        return this._http.post('account', profile, true);
    }

    Validate(username: string, type: string): Observable<any> {
        return this._http.post(`account/validate/${type}/${username}`, null, true);
    }

    DeRegister(userId: number) {
        return this._http.post(`account/dereg/${userId}`, null, true);
    }

    ResetPassword(data: any) {
        data.type = 'password';
        return this._http.post('account/reset', data, true);
    }

    ResetUsername(data: any) {
        data.type = 'username';
        return this._http.post('account/reset', data, true);
    }

    ResetUserPass(data: any) {
        return this._http.post('account/ResetPassword', data, true);
    }

    VerifyUser(token: string) {
        const data = {forgotId: token};
        return this._http.post('account/verify', data, true);
    }
}

export const LoginType = {
    Doctor: 'DOCTOR',
    Patient: 'PATIENT',
    Receptionist: 'RECEPTIONIST'
};
