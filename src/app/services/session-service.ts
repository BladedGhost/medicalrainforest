import { StorageConstants } from './../constants/storage-constants';
export class SessionService {
    public SetValue(key: string, value: any, setLocalStorage?: boolean) {
        if (setLocalStorage) {
            localStorage.setItem(key, JSON.stringify(value));
        } else {
            sessionStorage.setItem(key, JSON.stringify(value));
        }
    }

    public SetUserData(value: any, rememberMe: boolean) {
        localStorage.setItem(StorageConstants.RememberMe, JSON.stringify({rememberMe: rememberMe}));
        if (rememberMe) {
            localStorage.setItem(StorageConstants.UserSettings, JSON.stringify(value));
        } else {
            sessionStorage.setItem(StorageConstants.UserSettings, JSON.stringify(value));
        }
    }

    public GetUserData() {
        const rememberMe = JSON.parse(localStorage.getItem(StorageConstants.RememberMe)).rememberMe;
        if (rememberMe === undefined || rememberMe === null || rememberMe === false) {
            return JSON.parse(sessionStorage.getItem(StorageConstants.UserSettings));
        } else {
            return JSON.parse(localStorage.getItem(StorageConstants.UserSettings));
        }
    }

    public GetValue(key) {
        let item = JSON.parse(sessionStorage.getItem(key));
        if (!item || item === null) {
            item = JSON.parse(localStorage.getItem(key));
        }

        return item;
    }

    public ClearSession() {
        sessionStorage.clear();
        localStorage.clear();
    }
}
