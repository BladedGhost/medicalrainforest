import { Injectable } from '@angular/core';
import { HttpWrapperService } from './http-wrapper.service';

@Injectable()
export class DoctorService {

  constructor(private _http: HttpWrapperService) { }

  Create(data) {
    return this._http.put('Doctors', data, true);
  }

  Update(data) {
    return this._http.post('Doctors', data, true);
  }

  Get(doctorId) {
    return this._http.get(`Doctors/${doctorId}`, true);
  }

  GetPatientsDropDown(doctorId) {
    return this._http.get(`Doctors/patient/dropdown/${doctorId}`, true);
  }

  GetUid(userId) {
    return this._http.get(`Doctors/getuid/${userId}`, true);
  }

  GetMyPatients(doctorId) {
    return this._http.get(`Doctors/get/linkedPatients/${doctorId}`, true);
  }

  Delete(doctorId) {
    return this._http.delete(`Doctors/${doctorId}`, true);
  }

  DeletePatient(doctorId, patientId) {
    return this._http.delete(`Doctors/patient/${doctorId}/${patientId}`, true);
  }

  GetDoctorQRCode(doctorId) {
    return this._http.get(`Doctors/QR/${doctorId}`, true);
  }

  GetReceptionists(doctorId: number) {
    return this._http.get(`Doctors/receptionists/${doctorId}`, true);
  }

  DeleteReceptionists(userId: number) {
    return this._http.delete(`Doctors/receptionists/${userId}`, true);
  }
}
