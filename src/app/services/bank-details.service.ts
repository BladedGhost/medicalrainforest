import { HttpWrapperService } from './http-wrapper.service';
import { Injectable } from '@angular/core';

@Injectable()
export class BankDetailsService {

  constructor(private _http: HttpWrapperService) { }

  Create(data) {
    return this._http.put('BankDetails', data, true);
  }

  Update(data) {
    return this._http.post('BankDetails', data, true);
  }

  Get(bankDetailId) {
    return this._http.get(`BankDetails/get/${bankDetailId}`, true);
  }

  GetAll(doctorId) {
    return  this._http.get(`BankDetails/get/all/${doctorId}`, true);
  }

  GetAllPatient(userId) {
    return  this._http.get(`Patients/shared/bank/${userId}`, true);
  }

  LinkBankDetailWithPatient(bankDetailId, patientId) {
    return this._http.post(`BankDetails/link/${bankDetailId}/${patientId}`, null, true);
  }

  Delete(bankDetailId) {
    return this._http.delete(`BankDetails/${bankDetailId}`, true);
  }

  DeleteShared(bankDetailsPatientShareId) {
    return this._http.delete(`BankDetails/shared/${bankDetailsPatientShareId}`, true);
  }
}
