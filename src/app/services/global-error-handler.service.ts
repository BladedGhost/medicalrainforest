import { MessageService } from 'primeng/components/common/messageservice';
import { Injectable, Injector } from '@angular/core';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import * as StackTrace from 'stacktrace-js';

@Injectable()
export class GlobalErrorHandlerService {

  constructor(private injector: Injector) { }

  customHandleError(error, useStack: boolean) {
    const location = this.injector.get(LocationStrategy);
    console.log('Location', location);
    const message = error.message ? error.message : error.toString();
    console.log('Message', message);
    const url = location instanceof PathLocationStrategy ? location.path() : '';
    console.log('Url', url);

    if (useStack) {
      StackTrace.fromError(error).then(stackFrames => {
        const stackString = stackFrames
        .splice(0, 20)
        .map(function(sf) {
          return sf.toString();
        })
        .join('\n');
      });
      this.handleError(error);
    }
  }

  handleError(error) {
    console.log('Error', error);
  }
}
