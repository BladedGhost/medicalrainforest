import { HttpWrapperService } from './http-wrapper.service';
import { SessionService } from './session-service';
import { Injectable } from '@angular/core';

@Injectable()
export class NotificationService {

  constructor(private store: SessionService, private _http: HttpWrapperService) { }

  GetNotifications(userId: number, skipIds: Array<number>) {
    return this._http.get(`notification/${userId}/${skipIds}`, true);
  }

  DeleteNotifications(notidicationId: number) {
    return this._http.deleteNoNotify(`notification/${notidicationId}`, true);
  }
}
