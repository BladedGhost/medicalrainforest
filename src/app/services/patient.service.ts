import { HttpWrapperService } from './http-wrapper.service';
import { Injectable } from '@angular/core';

@Injectable()
export class PatientService {

  constructor(private _http: HttpWrapperService) { }

  CreatePatient(data) {
    return this._http.post('patients/create', data, true);
  }

  UpdatePatient(data) {
    return this._http.post('patients/update', data, true);
  }

  GetPatient(patientId) {
    return this._http.get(`patients/get/patient/${patientId}`, true);
  }

  GetPatientBase(patientBase, doctorId: number) {
    return this._http.get(`patients/get/patient/safe/${patientBase}/${doctorId}`, true);
  }

  GetPatients(userId) {
    return  this._http.get(`patients/get/${userId}`, true);
  }

  GetPatientsDropDown(userId) {
    return  this._http.get(`patients/get/dropdown/${userId}`, true);
  }

  LinkDoctorAndPatient(doctorCode: string, patientId) {
    return this._http.get(`patient/linkpatient/${patientId}/${doctorCode}`, true);
  }

  DeletePatient(patientId) {
    return this._http.delete(`patients/${patientId}`, true);
  }
}
