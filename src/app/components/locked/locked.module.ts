import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LockedComponent } from './locked.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [LockedComponent]
})
export class LockedModule { }
