import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth-services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-locked',
  templateUrl: './locked.component.html',
  styleUrls: ['./locked.component.css']
})
export class LockedComponent implements OnInit {

  constructor(private route: Router, private auth: AuthService) { }

  ngOnInit() {
  }

  goBack(e) {
    if (this.auth.isLoggedIn()) {
      this.route.navigateByUrl('dashboard');
    } else {
      this.route.navigateByUrl('');
    }
  }
}
