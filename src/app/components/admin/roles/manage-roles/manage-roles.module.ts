import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageRolesComponent } from './manage-roles.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ManageRolesComponent]
})
export class ManageRolesModule { }
