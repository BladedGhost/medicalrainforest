import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagePageComponent } from './manage-page.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ManagePageComponent]
})
export class ManagePageModule { }
