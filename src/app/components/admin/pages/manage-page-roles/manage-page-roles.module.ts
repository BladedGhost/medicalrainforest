import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagePageRolesComponent } from './manage-page-roles.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ManagePageRolesComponent]
})
export class ManagePageRolesModule { }
