import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePageRolesComponent } from './manage-page-roles.component';

describe('ManagePageRolesComponent', () => {
  let component: ManagePageRolesComponent;
  let fixture: ComponentFixture<ManagePageRolesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagePageRolesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePageRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
