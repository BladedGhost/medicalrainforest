import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/primeng';
import { SessionService } from '../../../services/session-service';
import { BankDetailsService } from '../../../services/bank-details.service';

@Component({
  selector: 'app-bank-details',
  templateUrl: './bank-details.component.html',
  styleUrls: ['./bank-details.component.css']
})
export class PatientBankDetailsComponent implements OnInit {

  constructor(private bankDetail: BankDetailsService, private _confirm: ConfirmationService,
    private store: SessionService) { }
  cols: Array<any>;
  dataSource: Array<any>;
  doctorId: string;
  user: any;

  ngOnInit() {
    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'surname', header: 'Surname' },
      { field: 'practiceNumber', header: 'Practice Number' },
      { field: 'branchName', header: 'Bank Name' },
      { field: 'branchCode', header: 'Branch Code' },
      { field: 'accountType', header: 'Account Type' },
      { field: 'accountNumber', header: 'Account Number' },
    ];

    this.user = this.store.GetUserData();

    this.getBankDetails();
  }

  getBankDetails() {
    this.bankDetail.GetAllPatient(this.user.data.userId).subscribe(x => {
      this.dataSource = x.json();
    });
  }

  deleteLinkedBank(e, item) {
    this._confirm.confirm({
      message: 'Are you sure you want to delete this bank details?',
      accept: () => this.bankDetail.DeleteShared(item.bankDetailsPatientShareId).subscribe(x => {
        this.getBankDetails();
      })
    });
  }
}
