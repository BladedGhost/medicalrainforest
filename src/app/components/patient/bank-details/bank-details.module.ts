import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientBankDetailsComponent } from './bank-details.component';
import { DataTableModule, PanelModule, ButtonModule, ConfirmDialogModule } from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    PanelModule,
    ButtonModule,
    ConfirmDialogModule
  ],
  declarations: [PatientBankDetailsComponent],
  exports: [PatientBankDetailsComponent]
})
export class PatientBankDetailsModule { }
