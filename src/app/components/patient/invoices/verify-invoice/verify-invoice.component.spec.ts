import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyInvoiceComponent } from './verify-invoice.component';

describe('VerifyInvoiceComponent', () => {
  let component: VerifyInvoiceComponent;
  let fixture: ComponentFixture<VerifyInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
