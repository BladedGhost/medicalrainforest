import { ButtonModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerifyInvoiceComponent } from './verify-invoice.component';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule
  ],
  declarations: [VerifyInvoiceComponent],
  exports: [VerifyInvoiceComponent]
})
export class VerifyInvoiceModule { }
