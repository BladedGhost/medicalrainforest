import { DocumentService } from './../../../../services/document.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-verify-invoice',
  templateUrl: './verify-invoice.component.html',
  styleUrls: ['./verify-invoice.component.css']
})
export class VerifyInvoiceComponent implements OnInit {
  @Output() verifyInvoice = new EventEmitter();
  @Output() cancel = new EventEmitter();

  @Input() invoiceId: number;

  constructor(private _invoice: DocumentService) { }

  ngOnInit() {
  }

  verifyClick(e, type) {
    this._invoice.UpdateInvoiceType(type, this.invoiceId).subscribe(x => {
      e.data = x.json();
      this.verifyInvoice.emit(e);
    });
  }

  cancelClick(e) {
    this.cancel.emit(e);
  }
}
