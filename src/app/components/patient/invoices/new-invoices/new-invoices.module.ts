import { VerifyInvoiceModule } from './../verify-invoice/verify-invoice.module';
import { DataTableModule, PanelModule, ButtonModule, DialogModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewInvoicesComponent } from './new-invoices.component';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    PanelModule,
    ButtonModule,
    DialogModule,
    VerifyInvoiceModule
  ],
  declarations: [NewInvoicesComponent],
  exports: [NewInvoicesComponent]
})
export class NewInvoicesModule {}
