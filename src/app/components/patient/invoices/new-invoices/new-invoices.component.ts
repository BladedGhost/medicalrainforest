import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../../services/session-service';
import { DocumentService } from '../../../../services/document.service';
import { VerifyInvoiceComponent } from '../verify-invoice/verify-invoice.component';

@Component({
  selector: 'app-new-invoices',
  templateUrl: './new-invoices.component.html',
  styleUrls: ['./new-invoices.component.css']
})
export class NewInvoicesComponent implements OnInit {
  cols: Array<any>;
  data: Array<any>;
  user: any;
  showVerifyInvoice: boolean;
  invoiceId: number;

  constructor(private _doc: DocumentService, private store: SessionService, private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.cols = [
      { field: 'doctor', header: 'Doctor' },
      { field: 'invoiceNumber', header: 'Invoice Number' },
      { field: 'dateInvoiced', header: 'Date Invoiced' },
      { field: 'invoiceType', header: 'Invoice Type' }
    ];

    this.user = this.store.GetUserData();
    this.getInvoices();

    this.activeRoute.params.subscribe(x => {
      if (x['id'] && x['id'] !== null && x['id'] >= 0) {
        this.invoiceId = x['id'];
        this._doc.CheckInvoiceVerified(this.invoiceId).subscribe(y => {
          if (y.json() === false) {
            this.showVerifyInvoice = true;
          }
        });
      }
    });
  }

  getInvoices() {
    this._doc.GetpatientInvoices(this.user.data.userId, 'new').subscribe(x => {
      this.data = x.json();
    });
  }

  DownloadInvoice(e, invoice) {
    this._doc.DownloadInvoice(invoice.invoiceId);
  }

  onFileTypeSelect(e, item, type) {
    this._doc.UpdateInvoiceType(type, item.invoiceId).subscribe(x => {
      this.getInvoices();
    });
  }

  onInvoiceVerify(e) {
    this.getInvoices();
    this.showVerifyInvoice = false;
  }

  onVerifyCancel(e) {
    this.showVerifyInvoice = false;
  }
}
