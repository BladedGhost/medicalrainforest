import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewInvoicesComponent } from './new-invoices.component';

describe('NewInvoicesComponent', () => {
  let component: NewInvoicesComponent;
  let fixture: ComponentFixture<NewInvoicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewInvoicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewInvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
