import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../../services/session-service';
import { DocumentService } from '../../../../services/document.service';

@Component({
  selector: 'app-outstanding-invoices',
  templateUrl: './outstanding-invoices.component.html',
  styleUrls: ['./outstanding-invoices.component.css']
})
export class OutstandingInvoicesComponent implements OnInit {
  cols: Array<any>;
  data: Array<any>;
  constructor(private _doc: DocumentService, private store: SessionService) { }

  ngOnInit() {
    this.cols = [
      { field: 'doctor', header: 'Doctor' },
      { field: 'invoiceNumber', header: 'Invoice Number' },
      { field: 'dateInvoiced', header: 'Date Invoiced' }
    ];

    const user = this.store.GetUserData();
    this._doc.GetpatientInvoices(user.data.userId, 'outstanding').subscribe(x => {
      this.data = x.json();
    });
  }

  DownloadInvoice(e, invoice) {
    this._doc.DownloadInvoice(invoice.invoiceId);
  }
}
