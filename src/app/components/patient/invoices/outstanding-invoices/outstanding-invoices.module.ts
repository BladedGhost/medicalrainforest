import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OutstandingInvoicesComponent } from './outstanding-invoices.component';
import { DataTableModule, PanelModule, ButtonModule } from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    PanelModule,
    ButtonModule
  ],
  declarations: [OutstandingInvoicesComponent],
  exports: [OutstandingInvoicesComponent]
})
export class OutstandingInvoicesModule { }
