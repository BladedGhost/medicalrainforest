import { DocumentService } from './../../../../services/document.service';
import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../../services/session-service';

@Component({
  selector: 'app-paid-invoices',
  templateUrl: './paid-invoices.component.html',
  styleUrls: ['./paid-invoices.component.css']
})
export class PaidInvoicesComponent implements OnInit {
  cols: Array<any>;
  data: Array<any>;
  constructor(private _doc: DocumentService, private store: SessionService) { }

  ngOnInit() {
    this.cols = [
      { field: 'doctor', header: 'Doctor' },
      { field: 'invoiceNumber', header: 'Invoice Number' },
      { field: 'dateInvoiced', header: 'Date Invoiced' },
      { field: 'datePaid', header: 'Paid On' }
    ];

    const user = this.store.GetUserData();
    this._doc.GetpatientInvoices(user.data.userId, 'paid').subscribe(x => {
      this.data = x.json();
    });
  }

  DownloadInvoice(e, invoice) {
    this._doc.DownloadInvoice(invoice.invoiceId);
  }
}
