import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaidInvoicesComponent } from './paid-invoices.component';
import { DataTableModule, PanelModule, ButtonModule } from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    PanelModule,
    ButtonModule
  ],
  declarations: [PaidInvoicesComponent],
  exports: [PaidInvoicesComponent]
})
export class PaidInvoicesModule { }
