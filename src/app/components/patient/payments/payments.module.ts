import { PaymentsComponent } from './payments.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelModule, ButtonModule } from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    PanelModule,
    ButtonModule
  ],
  declarations: [PaymentsComponent],
  exports: [PaymentsComponent]
})
export class PaymentsModule { }
