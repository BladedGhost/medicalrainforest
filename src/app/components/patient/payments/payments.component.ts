import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css']
})
export class PaymentsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  openZapper(e) {
      if (navigator.userAgent.toLowerCase().indexOf('android') > -1) {
        window.open('https://play.google.com/store/apps/details?id=com.zapper.android&hl=en');
      } else if (navigator.userAgent.toLowerCase().indexOf('iphone') > -1) {
        window.open('https://itunes.apple.com/app/zapper/id625240573?mt=8');
      } else {
        window.open('https://www.microsoft.com/en-za/store/p/zapper/9nblggh09fvb?rtc=1');
      }
  }
}
