import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageComponent } from './manage.component';
import { PanelModule, CardModule, ButtonModule, InputTextModule, StepsModule,
  InputSwitchModule, DropdownModule, CalendarModule, AutoCompleteModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    PanelModule,
    FormsModule,
    ReactiveFormsModule,
    CardModule,
    PanelModule,
    ButtonModule,
    InputTextModule,
    StepsModule,
    InputSwitchModule,
    DropdownModule,
    CalendarModule,
    AutoCompleteModule
  ],
  declarations: [ManageComponent],
  exports: [ManageComponent]
})
export class ManageModule { }
