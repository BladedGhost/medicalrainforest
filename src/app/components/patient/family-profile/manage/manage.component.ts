import { StorageConstants } from './../../../../constants/storage-constants';
import { SessionService } from './../../../../services/session-service';
import { PatientService } from './../../../../services/patient.service';
import { Observable } from 'rxjs/Rx';
import { LookupService } from './../../../../services/lookup-service';
import { SelectItem, MenuItem } from 'primeng/primeng';
import { Component, OnInit, Input } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder, FormControlName } from '@angular/forms';
import { Router } from '@angular/router';
import { MedicalAidService } from '../../../../services/medical-aid';

@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class ManageComponent implements OnInit {
  @Input() update: boolean;

  manageFormgroup: FormGroup;
  employmentStatus: SelectItem[];
  identificationType: SelectItem[];
  title: SelectItem[];
  gender: SelectItem[];
  wizardSteps: MenuItem[];
  hasMedicalAid: boolean;
  activeIndex = 0;
  lastStep = 1;
  member: any;
  medicalsectors: Array<any> = new Array<any>();
  dataSource: Array<any> = new Array<any>();

  createdCode = 'Blob';
  constructor(private fb: FormBuilder, private lookup: LookupService, private _patient: PatientService,
    private store: SessionService, private rout: Router, private medical: MedicalAidService) { }

  ngOnInit() {
    const user = this.store.GetUserData();
    const memberString = this.store.GetValue(StorageConstants.StoreData);
    this.member = memberString === '' || memberString === undefined ? {} : JSON.parse(memberString);

    this.manageFormgroup = this.fb.group({
      'patientId': new FormControl(0, []),
      'medicalAidId': new FormControl(0, []),
      'userId': new FormControl(user.data.userId, []),
      'updatedBy': new FormControl(user.username, []),
      'firstName': new FormControl('', Validators.required),
      'surname': new FormControl('', Validators.required),
      'titleId': new FormControl(0, []),
      'identificationTypeId': new FormControl('', Validators.required),
      'genderId': new FormControl('', []),
      'dob': new FormControl(new Date(), Validators.required),
      'age': new FormControl(0, []),
      'employmentStatusId': new FormControl(0, Validators.required),
      'occupation': new FormControl('', Validators.required),
      'emailAddress': new FormControl('', [Validators.required, Validators.email]),
      'telephoneNumber': new FormControl('', []),
      'workNumber': new FormControl('', []),
      'mobileNumber': new FormControl('', Validators.required),
      'emergencyContactName': new FormControl('', Validators.required),
      'emergencyContactNumber': new FormControl('', Validators.required),
      'emergencyContactRelationship': new FormControl('', Validators.required),
      'hasMedicalAid': new FormControl(false, []),
      'medicalaidsectorId': new FormControl(0, Validators.required),
      'medicalaidsector': new FormControl('', []),
      'medicalaidPlan': new FormControl('', Validators.required),
      'medicalAidNumber': new FormControl('', Validators.required),
      'medicalAidCode': new FormControl('', Validators.required),
      'postalAddress': new FormControl('', Validators.required),
      'mainMemberTitleId': new FormControl(0, Validators.required),
      'mainMemberSurname': new FormControl('', Validators.required),
      'mainMemberFirstName': new FormControl('', Validators.required),
      'mainMemberMobileNumber': new FormControl('', Validators.required),
      'image': new FormControl('', []),
      'imageData': new FormControl([], []),
      'mainMemberEmailAddress': new FormControl('', Validators.required),
      'uid': null,
      'gender': null,
      'title': null,
      'mainMemberTitle': null
    });
    this.checkMedicalAidShow(false);
    this.fillTypes();
    if (this.member !== null && this.member.patientId !== undefined) {
      this._patient.GetPatient(this.member.patientId).subscribe(x => {
        const data = x.json();
        data.dob = new Date(data.dob);
        this.checkMedicalAidShow(data.hasMedicalAid);
        this.manageFormgroup.setValue(data);
        this.manageFormgroup.patchValue({ 'updatedBy': user.username });
        this.manageFormgroup.updateValueAndValidity();
      });
    }
    this.fetchMedicalaid();
  }

  fetchMedicalaid() {
    this.medical.GetMedicalAids().map(s => {
      const items = <Array<SelectItem>>s.json().map(x => {
        return <SelectItem>{
          label: x.medicalAidName.toLowerCase().replace(/^(.)|\s(.)/g, (y) => y.toUpperCase()),
          value: x.medicalAidSectorId
        };
      });
      this.medicalsectors = s.json();
      return items;
    }).subscribe(x => {
      this.dataSource = x;
    });
  }

  onMedicalAidChange(e) {
    this.checkMedicalAidShow(e.checked);
  }

  checkMedicalAidShow(hasMedicalAid) {
    this.lastStep = hasMedicalAid ? 2 : 1;
    if (hasMedicalAid) {
      this.manageFormgroup.get('medicalaidsectorId').setValidators([Validators.required]);
      this.manageFormgroup.get('medicalaidPlan').setValidators([Validators.required]);
      this.manageFormgroup.get('medicalAidNumber').setValidators([Validators.required]);
      this.manageFormgroup.get('medicalAidCode').setValidators([Validators.required]);
      this.manageFormgroup.get('postalAddress').setValidators([Validators.required]);
      this.manageFormgroup.get('mainMemberTitleId').setValidators([Validators.required]);
      this.manageFormgroup.get('mainMemberSurname').setValidators([Validators.required]);
      this.manageFormgroup.get('mainMemberFirstName').setValidators([Validators.required]);
      this.manageFormgroup.get('mainMemberMobileNumber').setValidators([Validators.required]);
      this.manageFormgroup.get('mainMemberEmailAddress').setValidators([Validators.required]);
    } else {
      this.manageFormgroup.get('medicalaidsectorId').setValidators([]);
      this.manageFormgroup.get('medicalaidPlan').setValidators([]);
      this.manageFormgroup.get('medicalAidNumber').setValidators([]);
      this.manageFormgroup.get('medicalAidCode').setValidators([]);
      this.manageFormgroup.get('postalAddress').setValidators([]);
      this.manageFormgroup.get('mainMemberTitleId').setValidators([]);
      this.manageFormgroup.get('mainMemberSurname').setValidators([]);
      this.manageFormgroup.get('mainMemberFirstName').setValidators([]);
      this.manageFormgroup.get('mainMemberMobileNumber').setValidators([]);
      this.manageFormgroup.get('mainMemberEmailAddress').setValidators([]);
    }

    this.manageFormgroup.get('medicalaidsectorId').updateValueAndValidity();
    this.manageFormgroup.get('medicalaidPlan').updateValueAndValidity();
    this.manageFormgroup.get('medicalAidNumber').updateValueAndValidity();
    this.manageFormgroup.get('medicalAidCode').updateValueAndValidity();
    this.manageFormgroup.get('postalAddress').updateValueAndValidity();
    this.manageFormgroup.get('mainMemberTitleId').updateValueAndValidity();
    this.manageFormgroup.get('mainMemberSurname').updateValueAndValidity();
    this.manageFormgroup.get('mainMemberFirstName').updateValueAndValidity();
    this.manageFormgroup.get('mainMemberMobileNumber').updateValueAndValidity();
    this.manageFormgroup.get('mainMemberEmailAddress').updateValueAndValidity();

    this.setWizardSteps(hasMedicalAid);
  }

  setWizardSteps(hasMedical) {
    this.wizardSteps = [
      {
        label: 'Personal Information',
        command: (e) => {
          this.activeIndex = 0;
        }
      },
      {
        label: 'Emergency Information',
        command: (e) => {
          this.activeIndex = 1;
        }
      }
    ];

    if (hasMedical) {
      this.wizardSteps.push({
        label: 'Medical Aid',
        command: (e) => {
          this.activeIndex = 2;
        },
        visible: !this.manageFormgroup.controls['hasMedicalAid'].value === true,
        disabled: !this.manageFormgroup.controls['hasMedicalAid'].value === true
      });
    }
  }

  fillTypes() {
    this.getTypeData('Gender').subscribe(x => this.gender = x);
    this.getTypeData('EmployeeStatus').subscribe(x => this.employmentStatus = x);
    this.getTypeData('IdentificationType').subscribe(x => this.identificationType = x);
    this.getTypeData('Title').subscribe(x => this.title = x);
  }

  getTypeData(type) {
    return this.lookup.GetLookupsByType(type).map(s => {
      const items = <Array<SelectItem>>s.json().map(x => {
        return <SelectItem>{
          label: x.value,
          value: x.lookupValueId
        };
      });
      return items;
    });
  }

  next(e) {
    this.activeIndex++;
  }

  previous(e) {
    this.activeIndex--;
  }

  register(e) {
    if (!this.manageFormgroup.valid) {
      window.alert('Not all information is filled');
      return false;
    }

    if (this.member !== undefined && this.member !== null) {
      this._patient.UpdatePatient(this.manageFormgroup.value).subscribe(x => {
        if (x.ok === true) {
          this.rout.navigateByUrl('family-profile');
        } else {
          alert(x.statusText);
        }
      });
    } else {
      this._patient.CreatePatient(this.manageFormgroup.value).subscribe(x => {
        if (x.ok === true) {
          this.rout.navigateByUrl('family-profile');
        } else {
          alert(x.statusText);
        }
      });
    }
  }

  goBack(e) {
    this.rout.navigateByUrl('family-profile');
  }
}
