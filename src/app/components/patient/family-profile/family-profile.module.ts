import { ManageModule } from './manage/manage.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FamilyProfileComponent } from './family-profile.component';
import { DataGridModule, ButtonModule, PanelModule, CardModule, ConfirmDialogModule } from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    DataGridModule, ButtonModule, PanelModule, CardModule, ConfirmDialogModule
  ],
  declarations: [FamilyProfileComponent],
  exports: [FamilyProfileComponent]
})
export class FamilyProfileModule { }
