import { PatientService } from './../../../services/patient.service';
import { SessionService } from './../../../services/session-service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Card, ConfirmationService } from 'primeng/primeng';
import { PACKAGE_ROOT_URL } from '@angular/core/src/application_tokens';
import { StorageConstants } from '../../../constants/storage-constants';

@Component({
  selector: 'app-family-profile',
  templateUrl: './family-profile.component.html',
  styleUrls: ['./family-profile.component.css']
})
export class FamilyProfileComponent implements OnInit {
  family: Array<any>;

  constructor(private route: Router, private store: SessionService,
    private confirmServ: ConfirmationService, private _patient: PatientService) {
    this.getPatients();
    this.store.SetValue(StorageConstants.StoreData, '');
  }

  selectPerson(e, item) {
    this.store.SetValue(StorageConstants.StoreData, JSON.stringify(item));
    this.route.navigateByUrl('family-manage');
  }

  createPerson(e) {
    this.route.navigateByUrl('family-manage');
  }

  deletePerson(e, item) {
    this.confirm('Are you sure you want to delete this member?', (event) => {
      this._patient.DeletePatient(item.patientId).subscribe(x => {
        this.getPatients();
      });
    });
  }

  confirm(message, func: Function) {
    this.confirmServ.confirm({
      message: message,
      accept: func
    });
  }

  ngOnInit() {
  }
  getPatients() {
    const user = this.store.GetUserData();
    this._patient.GetPatients(user.data.userId).subscribe(x => {
      const items = x.json().map(m => {
        return {
          patientId: m.patientId,
          firstName: m.name,
          surname: m.surname,
          img: m.image === undefined || m.image === '' ? '../../../../assets/images/no-image.png' : m.image
        };
      });
      this.family = items;
    });
  }
}
