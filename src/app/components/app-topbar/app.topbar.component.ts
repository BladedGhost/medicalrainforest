import { Observable } from 'rxjs/Observable';
import { AuthService } from './../../services/auth-services';
import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app-component/app.component';
import { NotificationService } from '../../services/notification.service';
import { SessionService } from '../../services/session-service';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html',
    styleUrls: ['app.topbar.component.css']
})
export class AppTopBarComponent implements OnInit {
    userSettings: any;
    constructor(public app: AppComponent, private auth: AuthService, private route: Router,
        private notify: NotificationService, private store: SessionService) { this.checkUser(); }
    noti = [];
    user: any;
    notifySub: Subscription;
    ngOnInit() {
        this.user = this.store.GetUserData();
        this.checkNotifications();
    }

    checkNotifications() {
        const context = this;
        setTimeout(() => {
            this.notifySub = this.getNotification().subscribe(x => {
                this.noti = [];
                this.noti = Object.assign([], x.json());
                this.checkNotifications();
            });
        }, 2000);
    }

    getNotification() {
        return this.notify.GetNotifications(this.user.data.userId, null);
    }

    checkUser() {
        this.userSettings = this.auth.GetUserSettings();
        setTimeout(x => { if (this.userSettings === undefined) { this.checkUser(); } }, 1000);
    }

    logout(e) {
        this.auth.Logout();
    }

    executeCommand(e, item) {
        if (item.command !== '' && item.command !== null && item.command !== undefined) {

        }
    }

    routeToPage(e, item) {
        this.route.navigateByUrl(item.routerLink);
    }

    delete(e, item) {
        this.notify.DeleteNotifications(item.notificationId).subscribe(x => this.getNotification().subscribe());
        return false;
    }
}
