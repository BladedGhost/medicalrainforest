import {Component} from '@angular/core';

@Component({
    selector: 'app-footer',
    template: `
        <div class="layout-footer clearfix">
            <a href="dashboard.xhtml">
                <img alt="logo-colored" style="width:50px;" src="./assets/images/Outdoor.gif" />
                <label>The Medical RainForest</label>
            </a>
            <span class="footer-text-right">
                <span class="material-icons">copyright</span>
                <span>All Rights Reserved</span>
            </span>
        </div>
    `
})
export class AppFooterComponent {

}
