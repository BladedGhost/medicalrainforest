import { PanelMenuModule, ButtonModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArchiveComponent } from './archive.component';

@NgModule({
  imports: [
    CommonModule,
    PanelMenuModule,
    ButtonModule
  ],
  declarations: [ArchiveComponent],
  exports: [ArchiveComponent]
})
export class ArchiveModule { }
