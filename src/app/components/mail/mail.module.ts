import { DialogModule, ConfirmDialogModule, ButtonModule, ToolbarModule,
  SplitButtonModule, CardModule, PanelMenuModule, TabMenuModule, GrowlModule,
  DataTableModule, FileUploadModule, TooltipModule } from 'primeng/primeng';
import { MailComponent } from './mail.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { EditorModule } from 'primeng/components/editor/editor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    DialogModule,
    ConfirmDialogModule,
    ButtonModule,
    ToolbarModule,
    SplitButtonModule,
    CardModule,
    PanelMenuModule,
    TabMenuModule,
    InputTextModule,
    EditorModule,
    FormsModule,
    ReactiveFormsModule,
    GrowlModule,
    DataTableModule,
    FileUploadModule,
    TooltipModule
  ],
  declarations: [MailComponent],
  exports: [MailComponent]
})
export class MailModule { }
