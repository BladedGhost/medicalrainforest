import { MessageService } from 'primeng/components/common/messageservice';
import { MailService } from './../../services/mail.service';
import { SessionService } from './../../services/session-service';
import { EmailSimple } from './../../models/email-simple';
import { MenuItem, Message, FieldsetModule } from 'primeng/primeng';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-mail',
  templateUrl: './mail.component.html',
  styleUrls: ['./mail.component.css']
})
export class MailComponent implements OnInit {

  tabMenu: MenuItem[];
  emailList: MenuItem[] = new Array<MenuItem>();
  archive: MenuItem[];
  view = 'inbox';
  activeItem: MenuItem;
  showEditor: boolean;
  showAddWindow: boolean;
  emailGroup: FormGroup;
  user: any;
  selectedHtml: string;
  selectedItem: EmailSimple;
  subject = 'Untitled';
  msgs: Message[] = [];
  emailAdresses: string;
  addTo = 'To'; // can be To, CC or BCC
  toAddress = '';
  ccAddress = '';
  bccAddress = '';
  cols: Array<any>;
  data: Array<any>;
  files = '';

  constructor(private fb: FormBuilder, private store: SessionService, private email: MailService,
    private messageService: MessageService) { }

  ngOnInit() {
    this.user = this.store.GetUserData();
    this.emailGroup = this.fb.group({
      emailId: new FormControl(0, []),
      to: new FormControl('', [Validators.required, Validators.email]),
      from: new FormControl(this.user.data.email, []),
      cc: new FormControl('', []),
      subject: new FormControl('', [Validators.required]),
      body: new FormControl('', []),
      updatedBy: new FormControl(this.user.username),
      attachement: null
    });

    this.cols = [
      { field: 'fullName', header: 'Full Name' },
      { field: 'emailAddress', header: 'Email Address' }
    ];

    this.tabMenu = [
      {
        label: 'Inbox', command: (e) => this.changeView('inbox')
      },
      { label: 'Archive', command: (e) => this.changeView('archive') }
    ];

    this.getEmails(this.view);
    this.getMyLinkedPeople();
    this.activeItem = this.tabMenu[0];

    this.getMainEmailAddress();
  }

  getMainEmailAddress() {
    this.email.GetMainEmailAddress().subscribe(x => {
      const email = x.json().email;
      this.emailGroup.get('to').setValue(email);
    });
  }

  getMyLinkedPeople() {
    this.email.GetEmailPeople(this.user.data.userId, this.user.type).subscribe(x => {
      this.data = x.json();
    });
  }

  getFiles(event) {
    const reader = new FileReader();
    if (event.target.files.length > 0) {
      this.files = '';
      for (let i = 0; i < event.target.files.length; i++) {
        const file = event.target.files[i];
        if (this.files === '') { this.files = file.name; } else {
          this.files += ', ' + file.name;
        }
      }

      this.emailGroup.get('attachement').setValue(event.target.files);
    }
  }

  Select(e, data) {
    this.emailAdresses += this.emailAdresses === '' ? data.emailAddress : '; ' + data.emailAddress;
  }

  changeView(view) {
    this.view = view;
    this.getEmails(view);
  }

  openEmail(e) {
    this.selectedItem = e;
    this.selectedHtml = e.body;
    this.files = e.attachementFiles;
  }

  addToAddress(e) {
    switch (this.addTo) {
      case 'To':
        this.toAddress = this.toAddress === '' || this.toAddress === null ?
          this.emailAdresses :
          `${this.toAddress}; ${this.emailAdresses}`;
        break;
      case 'CC':
        this.ccAddress = this.ccAddress === '' || this.ccAddress === null ?
          this.emailAdresses :
          `${this.ccAddress}; ${this.emailAdresses}`;
        break;
      default:
        this.bccAddress = this.bccAddress === '' || this.bccAddress === null ?
          this.emailAdresses :
          `${this.bccAddress}; ${this.emailAdresses}`;
        break;
    }
    this.showAddWindow = false;
  }

  closeAdd(e) {
    this.showAddWindow = false;
    this.emailAdresses = '';
  }

  reply(e) {
    this.emailGroup.setValue({
      emailId: this.selectedItem.emailId,
      to: this.selectedItem.from,
      from: this.user.data.email,
      cc: this.selectedItem.cc,
      subject: 'Re: ' + this.selectedItem.subject,
      body: '<br/><br/><br/>-----------------------<br/><br/>' +
        this.selectedItem.body,
      updatedBy: this.user.username,
      attachement: null
    });

    this.emailAdresses = '';
    this.showEditor = true;
  }

  forward(e) {
    this.emailGroup.setValue({
      emailId: this.selectedItem.emailId,
      to: '',
      from: this.user.data.email,
      cc: this.selectedItem.cc,
      subject: 'Fw: ' + this.selectedItem.subject,
      body: '<br/><br/><br/>-----------------------<br/><br/>' +
        this.selectedItem.body + (this.files || this.files === null || this.files === '' ?
          '' :
          '<br/><br/><br/>-----------------------<br/><br/>Attachements:\
          <br/><br/>' + this.files),
      updatedBy: this.user.username,
      attachement: null
    });

    this.files = '';

    this.showEditor = true;
  }

  deleteEmail(e) {
    this.email.DeleteEmail(this.selectedItem.emailId).subscribe(x => {
      this.getEmails(this.view);
      this.selectedItem = undefined;
      this.selectedHtml = '';
    });
  }

  toAdd(e) {
    this.addTo = 'To';
    this.showAddWindow = true;
  }

  ccAdd(e) {
    this.addTo = 'CC';
    this.showAddWindow = true;
  }

  archiveEmail(e) {
    this.email.ArchiveEmail(this.selectedItem.emailId).subscribe(x => {
      this.getEmails(this.view);
      this.selectedItem = undefined;
      this.selectedHtml = '';
    });
  }

  openEditor(e) {
    this.showEditor = true;
  }

  private prepareData(): FormData {
    const data = new FormData();
    data.append('emailId', this.emailGroup.value.emailId);
    data.append('to', this.emailGroup.value.to);
    data.append('from', this.emailGroup.value.from);
    data.append('cc', this.emailGroup.value.cc || this.ccAddress);
    data.append('subject', this.emailGroup.value.subject);
    data.append('body', this.emailGroup.value.body);
    data.append('updatedBy', this.emailGroup.value.updatedBy);
    if (this.emailGroup.value.attachement && this.emailGroup.value.attachement !== null
      && this.emailGroup.value.attachement.length > 0) {
      for (let i = 0; i < this.emailGroup.value.attachement.length; i++) {
        const file = this.emailGroup.value.attachement[i];
        data.append('files', file);
      }
    }

    return data;
  }

  send(e) {
    const data = this.prepareData();
    this.email.SendEmailPlainManual(data, true).subscribe(x => {
      this.messageService.add({ severity: 'success', summary: 'Email', detail: 'Email has been sent successfully' });
      this.showEditor = false;
      this.subject = 'Untitled';
      this.emailGroup.setValue({
        to: '',
        from: this.user.data.email,
        cc: '',
        subject: '',
        body: '',
        updatedBy: this.user.username
      });

      this.getEmails(this.view);
    });
  }

  modelClose(e) {
    this.showEditor = false;
    this.subject = 'Untitled';
    this.emailGroup.setValue({
      to: '',
      from: this.user.data.email,
      cc: '',
      subject: '',
      body: '',
      updatedBy: this.user.username,
      attachement: null
    });
  }

  getEmails(emailType) {
    this.emailList = new Array<MenuItem>();
    this.email.GetEmail(this.user.data.email, emailType).subscribe(x => {
      const data = <Array<EmailSimple>>x.json();
      for (const emailitem of data) {
        const exist = this.emailList.filter(e => e.label === (emailitem.from === this.user.data.email ? 'Me' : emailitem.from));
        if (exist.length < 1) {
          const newEmail: MenuItem = {
            label: (emailitem.from === this.user.data.email ? 'Me' : emailitem.from),
            items: new Array<MenuItem>()
          };

          this.emailList.push(newEmail);
        }
      }

      this.emailList.forEach(e => {
        const newItems = data.filter(f => (f.from === this.user.data.email ? 'Me' : f.from) === e.label).map(m => {
          return <MenuItem>{
            label: m.subject,
            command: () => this.openEmail(m)
          };
        });

        e.items = newItems;
      });
    });
  }
}
