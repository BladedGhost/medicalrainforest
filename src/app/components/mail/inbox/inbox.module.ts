import { PanelMenuModule, ButtonModule } from 'primeng/primeng';
import { InboxComponent } from './inbox.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    PanelMenuModule,
    ButtonModule
  ],
  declarations: [InboxComponent],
  exports: [InboxComponent]
})
export class InboxModule { }
