import { SelectItem } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';
import { GlobalMessagesService } from './../../services/global-messages.service';
import { StorageConstants } from './../../constants/storage-constants';
import { SessionService } from './../../services/session-service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../../services/auth-services';
import { Component, OnInit, Inject } from '@angular/core';
import {Message} from 'primeng/components/common/api';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  showSignUp = false;
  showNewPass = false;
  showFetchUser = false;
  showResetPass = false;
  token: string;
  loginForm: FormGroup;
  msgs: Message[] = [];
  registrationType: SelectItem[];
  doctorType = 0;

  constructor(private auth: AuthService, private route: Router, private _fb: FormBuilder,
    private store: SessionService, @Inject(GlobalMessagesService) private toast: GlobalMessagesService,
    private activeRoute: ActivatedRoute, private messageService: MessageService) {
      document.body.style.backgroundImage = 'url(../../../assets/images/jungle2.jpg)';
      // document.getElementsByClassName('layout-main')[0].classList.remove('main-margin');
   }

  ngOnInit() {
    const userSignUp = this.store.GetValue(StorageConstants.SignUp);
    if (userSignUp !== undefined && userSignUp !== null && userSignUp === true) {
      this.signUp(null);
    }
    this.activeRoute.params.subscribe(x => {
      let verify = false;
      if (x['verify'] && x['verify'] !== null) {
        verify = true;
      }
      if (x['token'] && x['token'] !== null) {
        this.token = x['token'];
        if (!verify) {
          this.resetPassword(null);
        } else {
          this.auth.VerifyUser(this.token).subscribe(u => {
            if (u.json().verified === true) {
              this.messageService.add({severity: 'success', summary: 'Verify User', detail: 'User has been verified.'});
            } else {
              this.messageService.add({severity: 'error', summary: 'Verify User', detail: 'User could not be verified.'});
            }
          });
        }
      }
    });
    this.store.SetValue(StorageConstants.StoreData, 0);
    this.registrationType = [
      { label: 'Patient', value: 0 },
      { label: 'Doctor', value: 1 }
    ];

    this.loginForm = this._fb.group({
      'username': new FormControl('', Validators.required),
      'password': new FormControl('', Validators.required),
      'rememberMe': new FormControl(false, []),
      'hPCSANumber': new FormControl('', []),
      'practiceNumber': new FormControl('', []),
      'isDoctor': new FormControl(false, [])
    });
  }

  keyDownLogin(e) {
    if (e.keyCode === 13) {
      this.login(e);
    }
  }

  typeChanged(e) {
    if (e === 1) {
      this.loginForm.get('isDoctor').setValue(true);
      this.loginForm.get('hPCSANumber').setValidators([Validators.required]);
      this.loginForm.get('practiceNumber').setValidators([Validators.required]);
    } else {
      this.loginForm.get('isDoctor').setValue(false);
      this.loginForm.get('hPCSANumber').setValidators([]);
      this.loginForm.get('practiceNumber').setValidators([]);
    }

    this.loginForm.get('hPCSANumber').updateValueAndValidity();
    this.loginForm.get('practiceNumber').updateValueAndValidity();
  }

  login(e) {
    this.messageService.add({severity: 'information', summary: 'Sign In', detail: 'Logging in, Please be patient.'});
    this.auth.Login(this.loginForm.value);
  }

  signUp(e) {
    if (this.loginForm) {
      this.doctorType = this.loginForm.get('isDoctor').value === true ? 1 : 0;
    } else {
      this.doctorType = 0;
    }
    this.resetScreens();
    this.showSignUp = true;
  }

  fetchUser(e) {
    this.resetScreens();
    this.showFetchUser = true;
  }

  newPass(e) {
    this.resetScreens();
    this.showNewPass = true;
  }

  resetPassword (e) {
    this.resetScreens();
    this.showResetPass = true;
  }

  resetScreens() {
    this.showSignUp = false;
    this.showNewPass = false;
    this.showFetchUser = false;
    this.showResetPass = false;
  }

  cancel(e) {
    this.showSignUp = false;
  }

  submit(e) {
    this.messageService.add({severity: e.success ? 'success' : 'warning', summary: '', detail: e.message});
    this.resetScreens();
  }

  onFailed(e) {
    this.messageService.add({severity: 'error', summary: '', detail: e.message});
  }
}
