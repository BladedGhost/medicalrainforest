import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { CardModule, ButtonModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewPasswordComponent } from './new-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CardModule,
    InputTextModule,
    ButtonModule
  ],
  declarations: [NewPasswordComponent],
  exports: [NewPasswordComponent]
})
export class NewPasswordModule { }
