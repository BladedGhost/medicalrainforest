import { OnInit, Component, Output, EventEmitter, Input } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { PasswordValidation } from '../../../helpers/PasswordValidation';
import { AuthService } from '../../../services/auth-services';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.css']
})
export class NewPasswordComponent implements OnInit {
  @Input() token = '';
  @Output() onCancel = new EventEmitter();
  @Output() onSubmit = new EventEmitter();
  userform: FormGroup;
  constructor(private fb: FormBuilder, private auth: AuthService) { }

  ngOnInit() {
    this.userform = this.fb.group({
      'oldPassword': new FormControl((this.token && this.token !== null) ? 'ThisIsJustAPlaceHolder' : '', Validators.required),
      'password': new FormControl('', Validators.compose([Validators.required, Validators.minLength(8)])),
      'confirmPassword': new FormControl('', Validators.compose([Validators.required, Validators.minLength(8)])),
      'forgotId': new FormControl(this.token, [])
    }, { validator: PasswordValidation.MatchPassword });
  }

  reset(e) {
    this.auth.ResetUserPass(this.userform.value).subscribe(x => {
      const result = x.json();
      e.success = x;
      e.message = (x ?
                      'Password has been changed successfully' :
                      'Password could not be changed, please try again or contact your local administrator');
      if (x.json() === true) {
        this.onSubmit.emit(e);
      }
    });
  }

  cancel(e) {
    this.onCancel.emit(e);
  }
}
