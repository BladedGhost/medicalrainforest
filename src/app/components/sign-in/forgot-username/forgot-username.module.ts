import { CardModule, InputTextModule, ButtonModule } from 'primeng/primeng';
import { ForgotUsernameComponent } from './forgot-username.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CardModule,
    InputTextModule,
    ButtonModule
  ],
  declarations: [ForgotUsernameComponent],
  exports: [ForgotUsernameComponent]
})
export class ForgotUsernameModule { }
