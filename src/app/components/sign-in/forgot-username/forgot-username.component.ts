import { AuthService } from './../../../services/auth-services';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-forgot-username',
  templateUrl: './forgot-username.component.html',
  styleUrls: ['./forgot-username.component.css']
})
export class ForgotUsernameComponent implements OnInit {

  @Output() onCancel = new EventEmitter();
  @Output() onSubmit = new EventEmitter();
  forgotUserForm: FormGroup;
  constructor(private fb: FormBuilder, private auth: AuthService) { }

  ngOnInit() {
    this.forgotUserForm = this.fb.group({
      'idNumber': new FormControl(''),
      'email': new FormControl('', Validators.email)
    });
  }


  cancel(e) {
    this.onCancel.emit(e);
  }

  reset(e) {
    this.auth.ResetUsername(this.forgotUserForm.value).subscribe(x => {
      const result = x.json();
      e.success = x;
      e.message = x ?
                      'Username request has been sent successfully. If you have not received your email, please check your spam.' :
                      'We could not send your request, please try again or contact your local administrator.';
      this.onSubmit.emit(e);
    });
  }
}
