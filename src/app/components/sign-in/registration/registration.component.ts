import { StorageConstants } from './../../../constants/storage-constants';
import { MessageService } from 'primeng/components/common/messageservice';
import { SessionService } from './../../../services/session-service';
import { IdNumberValidation } from './../../../helpers/id-number-validation';
import { LookupService } from './../../../services/lookup-service';
import { GlobalMessagesService } from './../../../services/global-messages.service';
import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { SelectItem, InputText, Message, Card } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../services/auth-services';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { PasswordValidation } from '../../../helpers/PasswordValidation';
import { UsernameValidator } from '../../../helpers/UsernameValidator';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  discipline: SelectItem[];
  disableReg = true;
  isDoctor = '';
  registrationForm: FormGroup;
  acceptGroup: FormGroup;
  registering = false;

  @Input() receptionOnly: boolean;
  @Input() doctorType = 0;

  @Output() onCancel = new EventEmitter();
  @Output() onSubmit = new EventEmitter();
  @Output() onFailed = new EventEmitter();

  constructor(private message: GlobalMessagesService, private route: Router, private lookup: LookupService,
    private auth: AuthService, private fb: FormBuilder, public usernameValidator: UsernameValidator,
    private store: SessionService, private messageService: MessageService, private activeRoute: ActivatedRoute) {
  }

  showResponse(response) {
    return true;
  }
  submitForm(data) {

  }
  ngOnInit() {
    this.isDoctor = this.doctorType === 0 ? 'Patient' : 'Doctor';
    this.registrationForm = this.fb.group({
      'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2)])),
      'surname': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2)])),
      'email': new FormControl('', Validators.compose([Validators.required, Validators.email, Validators.minLength(6)])),
      'phone': new FormControl('', Validators.required),
      // 'username': new FormControl('', []),
      'username': new FormControl('', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(20)])),
        //  this.usernameValidator.checkUsername.bind(this.usernameValidator)),
      'password': new FormControl('', Validators.compose([Validators.required, Validators.minLength(8)])),
      'confirmPassword': new FormControl('', Validators.compose([Validators.required, Validators.minLength(8)])),
      'idpass': new FormControl('', Validators.required),
      'diciplineId': new FormControl('', Validators.required),
      'hpcsa': new FormControl('', Validators.required),
      'practice': new FormControl('', Validators.required),
      'practicePhone': new FormControl('', Validators.required),
      'doctorId': new FormControl(0, []),
      'acceptOnlineAppointments': new FormControl(false, []),
      'registeredFromApp': new FormControl(true, []),
      'role': []
  }, {validator: PasswordValidation.MatchPassword});

    this.acceptGroup = this.fb.group({
      'accept': new FormControl(false, Validators.requiredTrue)
    });
    this.checkTypeSection(this.doctorType);
    this.lookup.GetLookupsFormated('Dicipline').subscribe(x => {
      this.discipline = x;
    });
  }
  onAccept(e) {
    const checked = e.checked;
    this.disableReg = !checked;
  }

  register(e) {
    if (this.registrationForm.invalid) {
      alert('Please complete everything.');
      return false;
    }
    this.registering = true;
    this.auth.Register(this.registrationForm.value).subscribe(x => {
      this.registering = false;
      e.data = x.json();
      if (!e.data.success) {
        e.message = e.data.status;
        this.onFailed.emit(e);
        return;
      }
      e.success = true;
      e.message = 'Thank you for registering with The Medical Rainforest.';
      this.onSubmit.emit(e);
    });
  }

  cancel(e) {
    this.onCancel.emit(e);
  }

  typeChanged(e) {
    this.doctorType = e;
    this.isDoctor = e === 0 || this.receptionOnly ? 'Patient' : 'Doctor';
    this.checkTypeSection(e);
  }

  checkTypeSection(e) {
    this.registrationForm.get('role').setValue(e === 0 ? 'Patient' : 'Doctor');
    if (this.receptionOnly) {
      this.registrationForm.get('role').setValue('Receptionist');
      const user = this.store.GetUserData();
      this.registrationForm.get('doctorId').setValue(!!user ? user.data.doctorId : 0);
    }

    if (e === 0 || this.receptionOnly) {
      this.registrationForm.get('idpass').setValidators([]);
      this.registrationForm.get('diciplineId').setValidators([]);
      this.registrationForm.get('hpcsa').setValidators([]);
      this.registrationForm.get('practice').setValidators([]);
      this.registrationForm.get('practicePhone').setValidators([]);
    } else {
      this.registrationForm.get('idpass').setValidators(Validators.required);
      this.registrationForm.get('diciplineId').setValidators(Validators.required);
      this.registrationForm.get('hpcsa').setValidators(Validators.required);
      this.registrationForm.get('practice').setValidators(Validators.required);
      this.registrationForm.get('practicePhone').setValidators(Validators.required);
    }
    if (this.receptionOnly) {
      this.registrationForm.get('password').setValidators([]);
      this.registrationForm.get('confirmPassword').setValidators([]);
      this.acceptGroup.get('accept').setValidators([]);
    } else {
      this.registrationForm.get('password').setValidators([Validators.required, Validators.minLength(8)]);
      this.registrationForm.get('confirmPassword').setValidators([Validators.required, Validators.minLength(8)]);
      this.acceptGroup.get('accept').setValidators([Validators.requiredTrue]);
    }

    this.acceptGroup.get('accept').updateValueAndValidity();
    this.registrationForm.get('idpass').updateValueAndValidity();
    this.registrationForm.get('diciplineId').updateValueAndValidity();
    this.registrationForm.get('hpcsa').updateValueAndValidity();
    this.registrationForm.get('practice').updateValueAndValidity();
    this.registrationForm.get('practicePhone').updateValueAndValidity();
  }

}
