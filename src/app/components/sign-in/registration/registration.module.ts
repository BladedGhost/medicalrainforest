import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationComponent } from './registration.component';
import {
  InputTextModule, ButtonModule, SelectButtonModule, InputSwitchModule,
  TooltipModule, DropdownModule, CaptchaModule, CardModule, ProgressSpinnerModule
} from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    InputTextModule, ButtonModule, SelectButtonModule,
    InputSwitchModule, TooltipModule, CaptchaModule,
    DropdownModule, CardModule, ProgressSpinnerModule
  ],
  declarations: [RegistrationComponent],
  exports: [RegistrationComponent]
})
export class RegistrationModule { }
