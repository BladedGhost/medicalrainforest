import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from '../../../services/auth-services';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  @Output() onCancel = new EventEmitter();
  @Output() onSubmit = new EventEmitter();
  newPasswordForm: FormGroup;
  constructor(private fb: FormBuilder, private auth: AuthService) { }

  ngOnInit() {
    this.newPasswordForm = this.fb.group({
      'username': new FormControl('', Validators.required)
    });
  }


  cancel(e) {
    this.onCancel.emit(e);
  }

  reset(e) {
    this.auth.ResetPassword(this.newPasswordForm.value).subscribe(x => {
      const result = x.json();
      e.success = x;
      e.message = x ?
                      'Password request has been sent successfully. If you have not received your email, please check your spam.' :
                      'We could not send your request, please try again or contact your local administrator.';
      this.onSubmit.emit(e);
    });
  }
}
