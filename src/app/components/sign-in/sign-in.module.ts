import { NewPasswordModule } from './new-password/new-password.module';
import { RegistrationModule } from './registration/registration.module';
import { AuthService } from './../../services/auth-services';
import { SignInComponent } from './sign-in.component';
import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { CardModule, InputTextModule, ButtonModule, InputSwitchModule, GrowlModule } from 'primeng/primeng';
import { ForgotPasswordModule } from './forgot-password/forgot-password.module';
import { ForgotUsernameModule } from './forgot-username/forgot-username.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SelectButtonModule } from 'primeng/components/selectbutton/selectbutton';
@NgModule({
  imports: [
    CommonModule,
    CardModule,
    RegistrationModule,
    NewPasswordModule,
    ForgotPasswordModule,
    ForgotUsernameModule,
    InputTextModule,
    ReactiveFormsModule,
    FormsModule,
    ButtonModule,
    InputSwitchModule,
    GrowlModule,
    SelectButtonModule
  ],
  declarations: [SignInComponent],
  exports: [SignInComponent]
})
export class SignInModule implements OnInit {
  constructor(private auth: AuthService, private route: Router) {}

  ngOnInit() {

  }
}
