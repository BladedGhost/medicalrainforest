import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SchedulerComponent } from './scheduler.component';
import { ScheduleModule, ButtonModule, DialogModule, ConfirmDialogModule, CalendarModule,
    DropdownModule, InputTextareaModule } from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    ScheduleModule,
    ButtonModule,
    InputTextModule,
    InputTextareaModule,
    DialogModule,
    ConfirmDialogModule,
    CalendarModule,
    FormsModule,
    DropdownModule
  ],
  declarations: [SchedulerComponent],
  exports: [SchedulerComponent]
})
export class SchedulerModule { }
