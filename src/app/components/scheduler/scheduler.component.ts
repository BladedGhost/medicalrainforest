import { Days } from './../../constants/storage-constants';
import { DoctorService } from './../../services/doctor.service';
import { PatientService } from './../../services/patient.service';
import { SchedulersService } from './../../services/schedular-service';
import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../services/session-service';
import { StorageConstants } from '../../constants/storage-constants';

import * as moment from 'moment';
import { ConfirmationService } from 'primeng/primeng';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css']
})
export class SchedulerComponent implements OnInit {
  events: Array<any> = new Array<any>();
  showEvent = false;
  showInfoEvent = false;
  dateFrom: Date;
  dateFromString: string;
  doctorId: number;
  patientId: number;
  doctorSelected = false;
  showWeekends = true;
  scheduleId = 0;
  user: any;
  data: any;
  title: string;
  description: string;
  patientsData: Array<any> = new Array<any>();
  isDoctor = false;
  doctors: Array<any>;
  doctorOptions: any;
  canResize = false;
  defaultDate = moment();
  defaultView = 'month';
  mode = 'create';
  minTime: Date;
  maxTime: Date;
  disabledDays = [];
  businessHours = [{
    // days of week. an array of zero-based day of week integers (0=Sunday)
    dow: [1, 2, 3, 4, 5], // Monday - Thursday
    start: '8:00', // a start time (10am in this example)
    end: '17:00', // an end time (6pm in this example)
  }];
  header = {
    left: 'today prev,next',
    center: 'title',
    right: 'month,agendaWeek,agendaDay'
  };

  private changeMethod = () => {
    this.showEvent = false;
    if (this.user.data.doctorId !== undefined && this.user.data.doctorId !== null && this.user.data.doctorId > 0) {
      this.schedule.Get(this.user.data.doctorId).subscribe(y => {
        this.store.SetValue(StorageConstants.DoctorSchedule, JSON.stringify(y.json()));
        this.data = JSON.parse(this.store.GetValue(StorageConstants.DoctorSchedule));
      });
    } else {
      this.schedule.GetAll(this.user.data.userId).subscribe(y => {
        this.store.SetValue(StorageConstants.DoctorSchedule, JSON.stringify(y.json()));
        this.data = JSON.parse(this.store.GetValue(StorageConstants.DoctorSchedule));
        const d = { value: this.doctorId };
        this.setPatientDoctorSchedule(d);
      });
    }
  }

  constructor(private schedule: SchedulersService, private store: SessionService, private patients: PatientService,
    private _confirm: ConfirmationService, private activeRoute: ActivatedRoute, private doctor: DoctorService) { }

  ngOnInit() {
    const context = this;
    context.user = context.store.GetUserData();
    context.isDoctor = context.user.isDoctor;
    setTimeout(() => {
      if (context.isDoctor) {
        context.doctor.GetPatientsDropDown(context.user.data.doctorId).subscribe(x => {
          setTimeout(() => {
            context.patientsData = x.json();
          }, 1);
        });
      } else {
        context.patients.GetPatientsDropDown(context.user.data.userId).subscribe(x => {
          setTimeout(() => {
            context.patientsData = x.json();
          }, 1);
        });
      }
      context.data = JSON.parse(context.store.GetValue(StorageConstants.DoctorSchedule));
      context.getDoctors();

      if (context.isDoctor) {
        this.setDoctorSchedule();
      }

      context.activeRoute.params.subscribe(x => {
        if (x['date'] && x['date'] !== null) {
          context.defaultDate = moment(x['date']);
          context.defaultView = 'agendaDay';
        }
        if (x['docId'] && x['docId'] !== null) {
          context.doctorId = x['docId'];
          context.setPatientDoctorSchedule({value: context.doctorId});
        }
        if (x['id'] && x['id'] !== null) {
          const schedule = context.events.find(f => f.scheduleId === parseInt(x['id'], 0));
          schedule.start = moment(schedule.start);
          schedule.start = schedule.start.set({ 'hour': schedule.start.get('hour') + 2 });
          schedule.end = moment(schedule.end);
          context.openEvent({calEvent: schedule});
        }
      });
    }, 1);
    this.events = [];
  }

  createEvent(e) {
    this.mode = 'create';
    const isValidDay = this.checkValidDay(e.date);
    if (isValidDay) {
      this.showEvent = true;
      this.dateFrom = e.date.set({ 'hour': e.date.get('hour') - 2 }).toDate();
    }
  }

  checkValidDay(date): boolean {
    this.disabledDays = [];
    this.doctorOptions.workingdays.forEach(x => {
      if (!x.enabled) {
        this.disabledDays.push(Days[x.day]);
      }
    });
    const day = Days[date.day()];
    const selectedDayOption = this.doctorOptions.workingdays.find(x => x.day === day);
    if (!selectedDayOption.enabled || selectedDayOption.startTime === '') {
      return false;
    }

    this.minTime = moment().toDate();
    this.maxTime = moment().add(10, 'years').toDate();

    return true;
  }

  submit(e) {
    const dateTo = new Date(this.dateFrom.getTime() + 30 * 60000);
    const data = {
      doctorId: this.doctorId,
      patientId: this.patientId,
      title: this.title,
      description: this.description,
      dateStart: moment(this.dateFrom).set({ 'hour': moment(this.dateFrom).get('hour') + 2 }).toDate(),
      dateEnd: moment(dateTo).set({ 'hour': moment(dateTo).get('hour') + 2 }).toDate(),
      scheduleId: this.scheduleId
      // dateEnd: this.dateFrom.setMinutes(this.dateFrom.getMinutes() + 30).toLocaleString()
    };
    if (this.scheduleId > 0) {
      this.schedule.Update(data).subscribe(this.changeMethod);
    } else {
      this.schedule.Create(data).subscribe(this.changeMethod);
    }
  }

  selectPatient(e) {
    this.patientId = e.value;
  }

  getDoctors() {
    if (this.user.isDoctor) {
      this.events = this.data;
      this.checkDoctorDate(this.user.data.options);
    } else {
      this.schedule.GetDoctors(this.user.data.userId).subscribe(x => {
        this.doctors = x.json();
      });
    }
  }

  checkDoctorDate(data) {
    if (data === null || data === undefined) { return false; }
    data = JSON.parse(data);
    this.doctorOptions = data;
    this.businessHours = [];
    data.workingdays.forEach(x => {
      const day = parseInt(Days[x.day], 0);
      if (x.enabled) {
        this.businessHours.push({
          dow: [day],
          start: '',
          end: ''
        });
      }
    });

    data.leavedays.forEach(x => {
    });
  }

  updateEvent(e) {
    this.mode = 'update';
    this.showEvent = true;
    this.dateFrom = e.calEvent.start.set({ 'hour': e.calEvent.start.get('hour') - 2 }).toDate();
    this.description = e.calEvent.description;
    this.title = e.calEvent.title;
    this.scheduleId = e.calEvent.scheduleId;
    this.patientId = e.calEvent.patientId;
  }

  openEvent(e) {
    this.showInfoEvent = true;
    this.dateFrom = e.calEvent.start.set({ 'hour': e.calEvent.start.get('hour') - 2 }).toDate();
    this.dateFromString = e.calEvent.start.toISOString().replace(/T/g, ' ').replace(/Z/, '');
    this.description = e.calEvent.description;
    this.title = e.calEvent.title;
    this.scheduleId = e.calEvent.scheduleId;
    this.patientId = e.calEvent.patientId;
  }

  autoUpdate(e) {
    const data = {
      doctorId: e.event.doctorId,
      patientId: e.event.patientId,
      title: e.event.title,
      description: e.event.description,
      dateStart: e.event.start.toDate(),
      dateEnd: e.event.end.toDate(),
      scheduleId: e.event.scheduleId
    };
    this.schedule.Update(data).subscribe(this.changeMethod);
  }

  setPatientDoctorSchedule(e) {
    const selectedData = JSON.parse(JSON.stringify(this.data.filter(x => x.doctorId === parseInt(e.value, 0)))
      .replace(/date/g, '')
      .replace(/end/gi, 'end')
      .replace(/start/gi, 'start')
    );
    this.events = <any[]>selectedData.map(x => x.doctorSchedule)[0];
    this.doctorId = e.value;
    this.checkDoctorDate(this.doctors.find(x => x.value === e.value).data);
    this.doctorSelected = true;
  }

  setDoctorSchedule() {
    const selectedData = JSON.parse(JSON.stringify(this.data)
      .replace(/date/g, '')
      .replace(/end/gi, 'end')
      .replace(/start/gi, 'start')
    );
    this.events = selectedData.doctorSchedule;
  }

  delete() {
    this._confirm.confirm({
      message: 'Are you sure you want to delete this event?',
      accept: () => {
        this.schedule.Delete(this.scheduleId).subscribe(this.changeMethod);
      }
    });
  }

  close(e) {
    this.showEvent = false;
    this.showInfoEvent = false;
  }
}
