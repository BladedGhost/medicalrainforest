import { DoctorService } from './../../../services/doctor.service';
import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../services/session-service';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth-services';
import { StorageConstants } from '../../../constants/storage-constants';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-reception',
  templateUrl: './reception.component.html',
  styleUrls: ['./reception.component.css']
})
export class ReceptionComponent implements OnInit {
  dataSource: Array<any>;
  cols: Array<any>;
  user: any;
  constructor(private store: SessionService, private route: Router, private _auth: AuthService,
    private _doc: DoctorService, private messageService: MessageService) { }

  ngOnInit() {
    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'surname', header: 'Surname' },
      { field: 'username', header: 'Username' },
      { field: 'email', header: 'Email Address' },
    ];
    this.user = this.store.GetUserData();
    this.getReceptionists();
  }

  getReceptionists() {
    this._doc.GetReceptionists(this.user.data.doctorId).subscribe(x => {
      this.dataSource = x.json();
    });
  }

  add (e) {
    this.store.SetValue(StorageConstants.StoreData, {});
    this.route.navigateByUrl('reception/manage');
  }

  view(e, item) {
    this.store.SetValue(StorageConstants.StoreData, JSON.stringify(item));
  }

  reset(e, item) {
    this._auth.ResetUsername({type: 'username', username: item.username}).subscribe(x => {
      this.messageService.add({severity: 'success', summary: 'Password Reset', detail: `Email has been sent \
        to ${item.username} too reset their password`});
    });
  }

  delete (e, item) {
    this._doc.DeleteReceptionists(item.userId).subscribe(x => {
      this.getReceptionists();
    });
  }
}
