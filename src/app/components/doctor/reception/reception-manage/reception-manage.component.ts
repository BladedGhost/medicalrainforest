import { MessageService } from 'primeng/components/common/messageservice';
import { RegistrationComponent } from './../../../sign-in/registration/registration.component';
import { SignInComponent } from './../../../sign-in/sign-in.component';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reception-manage',
  templateUrl: './reception-manage.component.html',
  styleUrls: ['./reception-manage.component.css']
})
export class ReceptionManageComponent implements OnInit {

  constructor(private messageService: MessageService, private router: Router) { }

  ngOnInit() {
  }


  submit(e) {
    this.messageService.add({severity: e.success ? 'success' : 'warning', summary: '', detail: e.message});
    this.router.navigateByUrl('reception');
  }

  cancel(e) {
    this.router.navigateByUrl('reception');
  }
}
