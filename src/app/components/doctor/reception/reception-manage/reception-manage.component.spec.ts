import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceptionManageComponent } from './reception-manage.component';

describe('ReceptionManageComponent', () => {
  let component: ReceptionManageComponent;
  let fixture: ComponentFixture<ReceptionManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceptionManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceptionManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
