import { RegistrationModule } from './../../../sign-in/registration/registration.module';
import { SignInModule } from './../../../sign-in/sign-in.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceptionManageComponent } from './reception-manage.component';

@NgModule({
  imports: [
    CommonModule,
    SignInModule,
    RegistrationModule
  ],
  declarations: [ReceptionManageComponent],
  exports: [ReceptionManageComponent]
})
export class ReceptionManageModule { }
