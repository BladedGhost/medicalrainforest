import { PanelModule, ButtonModule, DataTableModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceptionComponent } from './reception.component';
import { ReceptionManageModule } from './reception-manage/reception-manage.module';

@NgModule({
  imports: [
    CommonModule,
    PanelModule,
    ButtonModule,
    DataTableModule,
    ReceptionManageModule
  ],
  declarations: [ReceptionComponent],
  exports: [ReceptionComponent]
})
export class ReceptionModule { }
