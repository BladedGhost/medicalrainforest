import { DocumentService } from './../../../services/document.service';
import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../services/session-service';
import { Router } from '@angular/router';
import { StorageConstants } from '../../../constants/storage-constants';
import { InvoiceEmailComponent } from './email/email.component';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class DoctorInvoicesComponent implements OnInit {
  data: Array<any>;
  user: any;
  cols: Array<any>;
  showEmail: boolean;
  fileItem: any;
  constructor(private store: SessionService, private doc: DocumentService, private _route: Router) { }

  ngOnInit() {
    this.cols = [
      { field: 'invoiceNumber', header: 'Invoice Number' },
      { field: 'fileName', header: 'Document Name' },
      { field: 'patient', header: 'Patient' },
      { field: 'paid', header: 'Paid' },
      { field: 'dateInvoiced', header: 'Date Invoiced' }
    ];
    this.user = this.store.GetUserData();
    this.store.SetValue(StorageConstants.StoreData, {});
    this.GetDocuments();
  }

  GetDocuments() {
    this.doc.GetDoctorInvoices(this.user.data.doctorId, 'all').subscribe(x => { this.data = x.json(); });
  }

  NewFile(e) {
    this.store.SetValue(StorageConstants.StoreData, {});
    this._route.navigateByUrl('doctor-invoices/manage');
  }

  email(e, item) {
    this.showEmail = true;
    this.fileItem = item;
  }

  onInvoiceSent(e) {
    this.showEmail = false;
  }

  onSendCancel() {
    this.showEmail = false;
  }

  markPaid(e, data, paid) {
    this.doc.UpdatePaid(data.invoiceId, paid).subscribe(x => {
      this.GetDocuments();
    });
  }

  DownloadFile(e, data) {
    this.doc.DownloadInvoice(data.invoiceId);
  }

  DeleteFile(e, data) {
    this.doc.DeleteInvoice(data.invoiceId).subscribe(x => {
      this.GetDocuments();
    });
  }
}
