import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageInvoicesComponent } from './manage-invoices.component';
import { InputTextModule, InputTextareaModule, PanelModule, CardModule, DropdownModule,
  ButtonModule, DialogModule, ConfirmDialogModule, FileUploadModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PatientShareModule } from '../../bank-details/patient-share/patient-share.module';

@NgModule({
  imports: [
    CommonModule,
    InputTextModule,
    InputTextareaModule,
    PanelModule,
    CardModule,
    DropdownModule,
    ButtonModule,
    FormsModule,
    ReactiveFormsModule,
    DialogModule,
    ConfirmDialogModule,
    PatientShareModule,
    FileUploadModule
  ],
  declarations: [ManageInvoicesComponent],
  exports: [ManageInvoicesComponent]
})
export class ManageInvoicesModule { }
