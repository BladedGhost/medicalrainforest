import { DoctorService } from './../../../../services/doctor.service';
import { SessionService } from './../../../../services/session-service';
import { Component, OnInit } from '@angular/core';
import { FileUpload, SelectItem } from 'primeng/primeng';
import { Router } from '@angular/router';
import { LookupService } from '../../../../services/lookup-service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-manage-invoices',
  templateUrl: './manage-invoices.component.html',
  styleUrls: ['./manage-invoices.component.css']
})
export class ManageInvoicesComponent implements OnInit {

  manageFormgroup: FormGroup;
  uploadedInvoices: any[] = [];
  user: any;
  uploadUrl = '';
  patientData: SelectItem[];
  constructor(private store: SessionService, private route: Router, private _lookup: LookupService, private fb: FormBuilder,
    private _doc: DoctorService) { }

  ngOnInit() {
    this.user = this.store.GetUserData();
    this.uploadUrl = this._lookup.GetUploadUrl() + '/invoice';
    this.manageFormgroup = this.fb.group({});
    this._doc.GetMyPatients(this.user.data.doctorId).map(x => {
      const data = x.json();
      const newData = data.map(d => {
        return <SelectItem>{ label: d.patient, value: d};
      });
      return newData;
    }).subscribe(x => {
      this.patientData = x;
    });
  }

  onSelect(event) {
    let doc: any;
    for (doc of event.files) {
      doc.doctorId = this.user.data.doctorId;
      doc.updatedBy = this.user.username;
      this.uploadedInvoices.push(doc);
    }
  }

  onUpload(e) {
    this.uploadedInvoices = [];
  }

  onBeforeSend(event) {
    event.xhr.setRequestHeader('Authorization', `Bearer ${this.user.data.token}`);
    const itemData = [];

    this.uploadedInvoices.forEach(x => {
      const item = {
        doctorId: x.doctorId,
        patientId: x.patientId,
        size: x.size,
        type: x.type,
        updatedBy: x.updatedBy,
        fileName: x.name,
        name: x.invoiceNumber
      };
      itemData.push(item);
    });
    event.formData.append('InvoiceData', JSON.stringify(itemData));
  }

  onClear(e) {
    this.uploadedInvoices = [];
  }

  removeFile(file: File, uploader: FileUpload) {
    const index = uploader.files.indexOf(file);
    uploader.remove(null, index);
    this.uploadedInvoices.splice(index, 1);
  }

  setPerson(e, data) {
    data.patientId = e.value.patientId;
  }

  changeInvoiceNumber(e, file) {
    file.invoiceNumber = e.target.value;
  }

  goBack(e) {
    this.route.navigateByUrl('doctor-invoices');
  }
}
