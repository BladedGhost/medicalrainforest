import { StorageConstants } from './../../../../constants/storage-constants';
import { SessionService } from './../../../../services/session-service';
import { MessageService } from 'primeng/components/common/messageservice';
import { MailService } from './../../../../services/mail.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-invoice-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class InvoiceEmailComponent implements OnInit {
  @Input() invoiceItem: any;
  @Output() invoiceSent = new EventEmitter();
  @Output() cancel = new EventEmitter();
  emailFg: FormGroup;
  text: string;
  results: string[];
  mainResults: string[];
  user: any;
  uid: string;

  constructor(private email: MailService, private fb: FormBuilder,
    private messageService: MessageService, private store: SessionService) { }

  ngOnInit() {
    this.user = this.store.GetUserData();
    this.emailFg = this.fb.group({
      message: new FormControl(null, []),
      subject: new FormControl(null, []),
      uid: new FormControl('', [Validators.required])
    });
    this.email.GetPatientUIds(this.user.data.doctorId).subscribe(x => {
      this.mainResults = x.json().patientIds;
    });
  }

  search(event) {
    const searchValue = event.query;
    this.results = this.mainResults.filter(x => {
      return x.indexOf(searchValue) > -1;
    });
  }

  sendEmail(e) {
    if (this.emailFg.valid) {
      this.invoiceItem.subject = this.emailFg.get('subject').value;
      this.invoiceItem.message = this.emailFg.get('message').value;
      this.invoiceItem.uid = this.uid;
      this.email.SendInvoiceItem(this.invoiceItem).subscribe(x => {
        e.data = this.invoiceItem;
        this.clearFields();
        this.messageService.add({ severity: 'success', summary: 'Invoice Email', detail: 'Invoice sent successfully.' });
        this.invoiceSent.emit(this.invoiceItem);
      });
    } else {
      this.messageService.add({
        severity: 'warning', summary: 'Invoice Email',
        detail: 'Please enter all required data before you can continue'
      });
    }
  }

  cancelSend() {
    this.clearFields();
    this.cancel.emit();
  }

  clearFields() {
    this.emailFg.get('subject').setValue('');
    this.emailFg.get('message').setValue('');
    this.emailFg.get('uid').setValue('');
    this.uid = '';
  }
}
