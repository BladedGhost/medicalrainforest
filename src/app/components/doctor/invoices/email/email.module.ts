import { InputTextModule, ButtonModule, PanelModule, InputTextareaModule, AutoCompleteModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceEmailComponent } from './email.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    InputTextModule,
    InputTextareaModule,
    ButtonModule,
    PanelModule,
    AutoCompleteModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [InvoiceEmailComponent],
  exports: [InvoiceEmailComponent]
})
export class InvoiceEmailModule { }
