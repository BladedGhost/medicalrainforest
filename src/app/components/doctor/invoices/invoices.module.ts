import { InvoiceEmailModule } from './email/email.module';
import { DoctorInvoicesComponent } from './invoices.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelModule, CardModule, ButtonModule, DataTableModule, FileUploadModule, DialogModule,
  ConfirmDialogModule, DropdownModule, InputTextModule } from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    PanelModule,
    CardModule,
    ButtonModule,
    DataTableModule,
    FileUploadModule,
    DialogModule,
    ConfirmDialogModule,
    DropdownModule,
    InputTextModule,
    InvoiceEmailModule
  ],
  declarations: [DoctorInvoicesComponent],
  exports: [DoctorInvoicesComponent]
})
export class DoctorInvoicesModule { }
