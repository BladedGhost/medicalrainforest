import { PatientShareComponent } from './patient-share/patient-share.component';
import { ConfirmationService } from 'primeng/primeng';
import { StorageConstants } from './../../../constants/storage-constants';
import { SessionService } from './../../../services/session-service';
import { BankDetailsService } from './../../../services/bank-details.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bank-details',
  templateUrl: './bank-details.component.html',
  styleUrls: ['./bank-details.component.css']
})
export class BankDetailsComponent implements OnInit {

  constructor(private bankDetail: BankDetailsService, private _confirm: ConfirmationService,
    private store: SessionService, private route: Router) { }
  cols: Array<any>;
  dataSource: Array<any>;
  doctorId: string;
  user: any;
  showShare: boolean;
  bankDetailsId = 0;

  ngOnInit() {
    this.store.SetValue(StorageConstants.StoreData, '');
    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'surname', header: 'Surname' },
      { field: 'practiceNumber', header: 'Practice Number' },
      { field: 'branchName', header: 'Bank Name' },
      { field: 'branchCode', header: 'Branch Code' },
      { field: 'accountType', header: 'Account Type' },
      { field: 'accountNumber', header: 'Account Number' },
    ];

    this.user = this.store.GetUserData();
    this.doctorId = this.user.data.doctorId;
    this.getBankDetails();
  }

  editAccount(e, item) {
    this.store.SetValue(StorageConstants.StoreData, JSON.stringify(item));
    this.route.navigateByUrl('bank-details/manage');
  }

  getBankDetails() {
    this.bankDetail.GetAll(this.user.data.doctorId).subscribe(x => {
      this.dataSource = x.json();
    });
  }

  share(e, item) {
    this.bankDetailsId = item.bankDetailsId;
    this.showShare = true;
  }

  deleteAccount(e, item) {
    this._confirm.confirm({
      message: 'Are you sure you want to delete this details?',
      accept: () => this.bankDetail.Delete(item.bankDetailsId).subscribe(x => { this.getBankDetails(); })
    });
  }

  addAccount(e) {
    this.route.navigateByUrl('bank-details/manage');
  }

  onSave(e) {
    this.showShare = false;
  }
}
