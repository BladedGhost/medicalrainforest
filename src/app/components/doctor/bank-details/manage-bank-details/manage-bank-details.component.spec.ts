import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageBankDetailsComponent } from './manage-bank-details.component';

describe('ManageBankDetailsComponent', () => {
  let component: ManageBankDetailsComponent;
  let fixture: ComponentFixture<ManageBankDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageBankDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageBankDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
