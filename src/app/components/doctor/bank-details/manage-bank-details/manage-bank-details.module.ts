import { PatientShareModule } from './../patient-share/patient-share.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageBankDetailsComponent } from './manage-bank-details.component';
import { InputTextModule, InputTextareaModule, PanelModule, CardModule,
  DropdownModule, ButtonModule, DialogModule, ConfirmDialogModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    InputTextModule,
    InputTextareaModule,
    PanelModule,
    CardModule,
    DropdownModule,
    ButtonModule,
    FormsModule,
    ReactiveFormsModule,
    DialogModule,
    ConfirmDialogModule,
    PatientShareModule
  ],
  declarations: [ManageBankDetailsComponent],
  exports: [ManageBankDetailsComponent]
})
export class ManageBankDetailsModule { }
