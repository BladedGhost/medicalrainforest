import { PatientShareComponent } from './../patient-share/patient-share.component';
import { ConfirmationService, SelectItem } from 'primeng/primeng';
import { LookupService } from './../../../../services/lookup-service';
import { GlobalMessagesService } from './../../../../services/global-messages.service';
import { BankDetailsService } from './../../../../services/bank-details.service';
import { SessionService } from './../../../../services/session-service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageConstants } from '../../../../constants/storage-constants';

@Component({
  selector: 'app-manage-bank-details',
  templateUrl: './manage-bank-details.component.html',
  styleUrls: ['./manage-bank-details.component.css']
})
export class ManageBankDetailsComponent implements OnInit {
  manageFormgroup: FormGroup;
  accountTypes: any;
  isView: boolean;
  doctorId: string;
  showShare: boolean;
  bankDetailsId = 0;
  banks: Array<any>;
  banksCollection: Array<any>;

  constructor(private fb: FormBuilder, private route: Router, private store: SessionService,
    private _bankDetails: BankDetailsService, private message: GlobalMessagesService,
    private _lookup: LookupService, private _confirm: ConfirmationService) { }

  ngOnInit() {
    const user = this.store.GetUserData();
    this.manageFormgroup = this.fb.group({
      'bankDetailsId': new FormControl(0, [Validators.required]),
      'name': new FormControl('', [Validators.required]),
      'surname': new FormControl('', [Validators.required]),
      'practiceNumber': new FormControl('', [Validators.required]),
      'branchName': new FormControl('', [Validators.required]),
      'branchCode': new FormControl('', [Validators.required]),
      'accountTypeId': new FormControl(0, [Validators.required]),
      'accountType': new FormControl(0, [Validators.required]),
      'accountNumber': new FormControl('', [Validators.required]),
      'notes': new FormControl('', []),
      'updatedBy': new FormControl(user.username, []),
      'doctorId': new FormControl(user.data.doctorId, [])
    });

    this.doctorId = user.data.doctorId;

    this._lookup.GetLookupsFormated('AccountType').subscribe(x => {
      this.accountTypes = x;
    });

    const stringData = this.store.GetValue(StorageConstants.StoreData);
    if (stringData !== '' && stringData !== undefined && stringData !== null) {
      const data = JSON.parse(stringData);
      if (data) {
        this.isView = true;
        this.manageFormgroup.setValue(data);
      }
    }

    this.getBanksStuff();
  }

  backToBankDetails(e) {
    this.route.navigateByUrl('bank-details');
  }

  getBanksStuff() {
    this._lookup.GetLookupsByType('Banks').subscribe(x => {
      const items = <Array<SelectItem>>x.json().map(s => {
        return <SelectItem>{
          label: s.value,
          value: s.value
        };
      });
      this.banks = items;
      this.banksCollection = x.json();
    });
  }

  setBankCode(e) {
    const code = this.banksCollection.find(x => x.value === e.value).value2;
    this.manageFormgroup.get('branchCode').setValue(code);
  }

  saveDetails(e) {
    if (!this.manageFormgroup.valid) {

      return false;
    }
    if (this.isView) {
      this._bankDetails.Update(this.manageFormgroup.value).subscribe(x => {
        this.route.navigateByUrl('bank-details');
      });
    } else {
      this._bankDetails.Create(this.manageFormgroup.value).subscribe(x => {
        this.route.navigateByUrl('bank-details');
      });
    }
  }

  deleteDetails(e) {
    this._confirm.confirm({
      message: 'Are you sure you want to delete this details?',
      accept: () => this._bankDetails.Delete(this.manageFormgroup.value.bankDetailsId).subscribe(x => {
        this.route.navigateByUrl('bank-details');
      })
    });
  }

  shareDetails(e) {
    this.bankDetailsId = this.manageFormgroup.value.bankDetailsId;
    this.showShare = true;
  }
}
