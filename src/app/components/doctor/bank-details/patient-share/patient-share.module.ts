import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientShareComponent } from './patient-share.component';
import { DropdownModule, ButtonModule } from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    DropdownModule,
    ButtonModule,
    FormsModule
  ],
  declarations: [PatientShareComponent],
  exports: [PatientShareComponent]
})
export class PatientShareModule { }
