import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientShareComponent } from './patient-share.component';

describe('PatientShareComponent', () => {
  let component: PatientShareComponent;
  let fixture: ComponentFixture<PatientShareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientShareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
