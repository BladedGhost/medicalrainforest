import { DoctorService } from './../../../../services/doctor.service';
import { SessionService } from './../../../../services/session-service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BankDetailsService } from '../../../../services/bank-details.service';
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-patient-share',
  templateUrl: './patient-share.component.html',
  styleUrls: ['./patient-share.component.css']
})
export class PatientShareComponent implements OnInit {
  @Input() bankDetailsId;
  @Output() onSave = new EventEmitter();
  patientId: any;
  patient: SelectItem[];
  constructor(private store: SessionService, private bank: BankDetailsService, private doc: DoctorService) { }

  ngOnInit() {
    const user = this.store.GetUserData();
    this.doc.GetMyPatients(user.data.doctorId).map(x => {
      const data = x.json();
      const newData = data.map(d => {
        return <SelectItem>{ label: d.firstName + ' ' + d.surname, value: d};
      });
      return newData;
    }).subscribe(x => {
      this.patient = x;
    });
  }

  setPerson(e) {
    this.patientId = e.value.patientId;
  }

  linkBankDetails(e) {
    this.bank.LinkBankDetailWithPatient(this.bankDetailsId, this.patientId).subscribe(x => {
      this.onSave.emit(x);
    });
  }
}
