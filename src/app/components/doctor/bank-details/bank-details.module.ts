import { PatientShareModule } from './patient-share/patient-share.module';
import { BankDetailsComponent } from './bank-details.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableModule, ButtonModule, ConfirmDialogModule, CardModule, PanelModule, InputTextModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/components/dialog/dialog';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    ButtonModule,
    DialogModule,
    ConfirmDialogModule,
    CardModule,
    PanelModule,
    PatientShareModule,
    InputTextModule
  ],
  declarations: [BankDetailsComponent],
  exports: [BankDetailsComponent]
})
export class BankDetailsModule { }
