import { DoctorService } from './../../../services/doctor.service';
import { SessionService } from './../../../services/session-service';
import { Component, OnInit } from '@angular/core';
import { StorageConstants } from '../../../constants/storage-constants';

@Component({
  selector: 'app-barcode',
  templateUrl: './barcode.component.html',
  styleUrls: ['./barcode.component.css']
})
export class BarcodeComponent implements OnInit {
  qrCode: string;
  location: string;
  doc: string;
  user: any;
  printQR: boolean;
  constructor(private store: SessionService, private _doctor: DoctorService) { }

  ngOnInit() {
    this.user = this.store.GetUserData();
    this._doctor.GetDoctorQRCode(this.user.data.doctorId).subscribe(x => {
      this.location = x.json().location;
      this.doc = x.json().doctor;
      this.qrCode = x.json().code;
    });
  }

  print(e): void {
    let printContents, popupWin;
    const doc = this.user.username;
    printContents = document.getElementById('print-section').innerHTML;
    const siteQrCode = document.getElementById('siteQrCode').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>${doc} QR Code</title>
          <style>
            img {
              width: 100%;
            }
            .heading {
              display:block;
              width:100%;
              padding:20px 0;
              text-align:center;
              font-size:.8em;
            }
            .footer {
              position:fixed;
              bottom:0;
              padding:20px 0;
              text-align:center;
              width:100%;
              font-size:.8em;
            }
          </style>
        </head>
        <body onload="window.print();window.close()">
        <div class="heading">
          <p style="font-weight:bolder;border-bottom:1px solid #dfdfdf;">${this.doc}<br/>${this.location}</p>
          <p>
            The Medical Rainforest TM - Your very own Medical Assistant
          </p>
          <p>
            Register today - Its Free - Take The Filling In Of Forms To The Next Level
          </p>
          <p>Let Your Patients Download The App For Free!</p>
        </div>
        ${printContents}
        <div class="footer">
        <div>
            <div>
            </div>
        </div>
          <p>
            The Medical Rainforest TM - Your very own Medical Assistant
          </p>
          <p>
            Register today - Its Free - Take The Filling In Of Forms To The Next Level
          </p>
          <p>
            Let Your Patients Download The App For Free!
          </p>
        </div>
        </body>
      </html>`
    );
    popupWin.document.close();
  }
}
