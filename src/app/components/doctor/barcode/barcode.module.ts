import { ButtonModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BarcodeComponent } from './barcode.component';
import { NgxQRCodeModule } from 'ngx-qrcode2';

@NgModule({
  imports: [
    CommonModule,
    NgxQRCodeModule,
    ButtonModule
  ],
  declarations: [BarcodeComponent],
  exports: [BarcodeComponent]
})
export class BarcodeModule { }
