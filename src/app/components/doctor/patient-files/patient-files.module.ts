import { BarcodeModule } from './../barcode/barcode.module';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { DialogModule } from 'primeng/components/dialog/dialog';
import { ButtonModule, PanelModule, ConfirmDialogModule, DataTableModule, CardModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientFilesComponent } from './patient-files.component';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    PanelModule,
    DialogModule,
    ConfirmDialogModule,
    InputTextModule,
    DataTableModule,
    BarcodeModule,
    CardModule
  ],
  declarations: [PatientFilesComponent],
  exports: [PatientFilesComponent]
})
export class PatientFilesModule { }
