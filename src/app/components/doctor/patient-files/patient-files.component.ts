import { Observable } from 'rxjs/Rx';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { DoctorService } from './../../../services/doctor.service';
import { SessionService } from './../../../services/session-service';
import { BarcodeComponent } from './../barcode/barcode.component';
import { Component, OnInit } from '@angular/core';
import { StorageConstants } from '../../../constants/storage-constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patient-files',
  templateUrl: './patient-files.component.html',
  styleUrls: ['./patient-files.component.css']
})
export class PatientFilesComponent implements OnInit {
  showDialog: boolean;
  myPatients: Array<any>;
  user: any;
  data: Array<any>;
  tempData: Array<any>;
  cols: Array<any>;

  constructor(private store: SessionService, private _doctor: DoctorService, private route: Router,
  private _confirm: ConfirmationService) { }

  ngOnInit() {
    this.user = this.store.GetUserData();

    this.cols = [
      { field: 'patient', header: 'Full Name' },
      { field: 'sharePatient', header: 'Share Patient' },
      { field: 'sharePersonal', header: 'Share Personal Details' },
      { field: 'shareEmergencyDetails', header: 'Share Emergengy' },
      { field: 'shareMedicalAid', header: 'Share Medical Aid' }
    ];

    this.loadPatients();
  }

  loadPatients() {
    return this._doctor.GetMyPatients(this.user.data.doctorId).subscribe(x => {
      this.myPatients = x.json();
      this.tempData = x.json();
    });
  }

  openPatient(e, item) {
    this._doctor.GetUid(item.userId).subscribe(x => {
      this.store.SetValue('uid', JSON.stringify(x.json()));
      this.store.SetValue(StorageConstants.StoreData, JSON.stringify(item));
      this.route.navigateByUrl('patient-files/patient');
    });
  }

  filter(e) {
    this.myPatients = this.tempData;
    if (e.target.value && e.target.value !== null && e.target.value !== '') {
      this.myPatients = this.tempData.filter(x => {
        return x.patient.indexOf(e.target.value) > -1;
      });
    }
  }

  deletePatient(e, item) {
    this._confirm.confirm({
      message: 'Are you sure you want to remove this patient?',
      accept: () => {
        this._doctor.DeletePatient(this.user.data.doctorId, item.patientId).subscribe(x => {
          this.loadPatients();
        });
      }
    });
  }
}
