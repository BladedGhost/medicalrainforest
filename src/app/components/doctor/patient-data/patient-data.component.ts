import { PatientService } from './../../../services/patient.service';
import { Router, ActivatedRoute } from '@angular/router';
import { StorageConstants } from './../../../constants/storage-constants';
import { SessionService } from './../../../services/session-service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-patient-data',
  templateUrl: './patient-data.component.html',
  styleUrls: ['./patient-data.component.css']
})
export class PatientDataComponent implements OnInit {

  patientData: any = {};
  fullData: any = {};
  qrCode: string;
  uid: string;
  token: string;
  user: any;

  constructor(private store: SessionService, private _route: Router, private activeRoute: ActivatedRoute,
    private patient: PatientService) { }

  ngOnInit() {
    if (this.store.GetValue(StorageConstants.StoreData)) {
      this.fullData = JSON.parse(this.store.GetValue(StorageConstants.StoreData));
      this.patientData = this.fullData.patientData;
      this.uid = this.patientData.uid;
    }
    this.user = this.store.GetUserData();
    this.activeRoute.params.subscribe(x => {
      if (x['token'] && x['token'] !== null) {
        this.token = x['token'];
        this.patient.GetPatientBase(this.token, this.user.data.doctorId).subscribe(p => {
          this.store.SetValue(StorageConstants.StoreData, JSON.stringify(p.json()));
          this.fullData = JSON.parse(this.store.GetValue(StorageConstants.StoreData));
          this.patientData = this.fullData.patientData;
          this.uid = this.patientData.uid;
        });
      }
    });
  }

  back() {
    this._route.navigateByUrl('patient-files');
  }

  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <style>
          .ui-md-3{
            width: 30%;
            float:left;
          }
          .ui-card-title {
            clear:both;
            width:100%;
            display:block;
          }

          .ui-card-title {
            margin-top: 20px !important;
            margin-bottom: 10px !important;
            display: inline-block;
            font-weight: bolder;
            font-size: 1.4em;
            border-bottom: 1px solid #dfdfdf;
          }

          .uid {
            border-bottom: 1px solid #dfdfdf;
          }

          h3{padding:0;margin:0;margin-top:2px;}
            img {
              width: 100%;
            }
          .heading {
            display:block;
            width:100%;
            padding:20px 0;
            text-align:center;
            font-size:.8em;
          }
          .footer {
            position:fixed;
            bottom:0;
            padding:20px 0;
            text-align:center;
            width:100%;
            font-size:.8em;
          }
          </style>
        </head>
    <body onload="window.print();window.close()">
    ${printContents}
    </body>
      </html>`
    );
    popupWin.document.close();
  }
}
