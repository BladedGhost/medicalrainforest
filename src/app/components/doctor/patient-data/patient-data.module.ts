import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientDataComponent } from './patient-data.component';
import { CardModule, ButtonModule, InputTextModule, PanelModule } from 'primeng/primeng';
import { NgxQRCodeModule } from 'ngx-qrcode2';

@NgModule({
  imports: [
    CommonModule,
    CardModule,
    ButtonModule,
    InputTextModule,
    PanelModule,
    NgxQRCodeModule
  ],
  declarations: [PatientDataComponent],
  exports: [PatientDataComponent]
})
export class PatientDataModule { }
