import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth-services';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  constructor(private route: Router, private auth: AuthService) { }

  ngOnInit() {
  }
  goBack(e) {
    if (this.auth.isLoggedIn()) {
      this.route.navigateByUrl('dashboard');
    } else {
      this.route.navigateByUrl('');
    }
  }
}
