import { PanelModule, CardModule, ButtonModule, DataTableModule,
  FileUploadModule, DialogModule, ConfirmDialogModule,
  DropdownModule, InputTextModule} from 'primeng/primeng';
import { DocumentsComponent } from './documents.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    PanelModule,
    CardModule,
    ButtonModule,
    DataTableModule,
    FileUploadModule,
    DialogModule,
    ConfirmDialogModule,
    DropdownModule,
    InputTextModule
  ],
  declarations: [DocumentsComponent],
  exports: [DocumentsComponent]
})
export class DocumentsModule { }
