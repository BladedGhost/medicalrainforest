import { Router } from '@angular/router';
import { StorageConstants } from './../../constants/storage-constants';
import { DocumentService } from './../../services/document.service';
import { SessionService } from './../../services/session-service';
import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {

  cols: Array<any>;
  data: Array<any>;
  constructor(private store: SessionService, private _doc: DocumentService, private _route: Router,
    private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.cols = [
      { field: 'documentId', header: 'Document No' },
      { field: 'documentName', header: 'Document Name' },
      { field: 'documentType', header: 'Document Type' },
      { field: 'name', header: 'Name' },
      { field: 'surname', header: 'Surname' },
      { field: 'fileType', header: 'File Type' },
      { field: 'createdBy', header: 'Uploaded By' },
      { field: 'dateCreated', header: 'Date Created' }
    ];
    this.fetchFiles();
    this.store.SetValue(StorageConstants.StoreData, {});
  }

  NewFile(e) {
    this.store.SetValue(StorageConstants.StoreData, {});
    this._route.navigateByUrl('file-manager');
  }

  DownloadFile($event, file) {
    this._doc.DownloadFile(file.documentId);
  }

  EditFile($event, file) {
    this._doc.GetFile(file.DocumentId).subscribe(x => {
      this.store.SetValue(StorageConstants.StoreData, JSON.stringify(x.json()));
      this._route.navigateByUrl('file-manager');
    });
  }

  DeleteFile(e, file) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this file?',
      accept: () => {
        this._doc.DeleteFile(file.documentId).subscribe(x => {
          this.fetchFiles();
        });
      }
    });
  }

  fetchFiles() {
    const user = this.store.GetUserData();
    this._doc.GetAll(user.data.userId).subscribe(x => {
      this.data = x.json();
    });
  }
}
