import { ButtonModule, FileUploadModule, DropdownModule, InputTextModule,
  CardModule, PanelModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileManagerComponent } from './file-manager.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    FileUploadModule,
    DropdownModule,
    InputTextModule,
    CardModule,
    PanelModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [FileManagerComponent],
  exports: [FileManagerComponent]
})
export class FileManagerModule { }
