import { LookupService } from './../../../services/lookup-service';
import { StorageConstants } from './../../../constants/storage-constants';
import { SessionService } from './../../../services/session-service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FileUpload } from 'primeng/primeng';
import { SelectItem } from 'primeng/components/common/selectitem';
import { Router } from '@angular/router';

@Component({
  selector: 'app-file-manager',
  templateUrl: './file-manager.component.html',
  styleUrls: ['./file-manager.component.css']
})
export class FileManagerComponent implements OnInit {

  manageFormgroup: FormGroup;
  uploadedFiles: any[] = [];
  user: any;
  documentType: SelectItem[];
  uploadUrl = '';

  constructor(private fb: FormBuilder, private store: SessionService, private _lookup: LookupService, private route: Router) { }

  ngOnInit() {
    this.manageFormgroup = this.fb.group({

    });
    this._lookup.GetLookupsFormated('DocumentType').subscribe(x => {
      this.documentType = x;
    });
    this.user = this.store.GetUserData();
    this.uploadUrl = this._lookup.GetUploadUrl();
  }

  onSelect(event) {
    let doc: any;
    for (doc of event.files) {
      doc.userId = this.user.data.userId;
      doc.updatedBy = this.user.username;
      this.uploadedFiles.push(doc);
    }
  }

  onUpload(e) {
    this.uploadedFiles = [];
  }

  onBeforeSend(event) {
    event.xhr.setRequestHeader('Authorization', `Bearer ${this.user.data.token}`);
    const itemData = [];

    this.uploadedFiles.forEach(x => {
      const item = {
        userId: x.userId,
        documentTypeId: x.documentTypeId,
        size: x.size,
        type: x.type,
        updatedBy: x.updatedBy,
        fileName: x.name,
        name: x.fullName,
        surname: x.surname
      };
      itemData.push(item);
    });
    event.formData.append('DocumentData', JSON.stringify(itemData));
  }

  onClear(e) {
    this.uploadedFiles = [];
  }

  removeFile(file: File, uploader: FileUpload) {
    const index = uploader.files.indexOf(file);
    uploader.remove(null, index);
    this.uploadedFiles.splice(index, 1);
  }

  setFileType(e, data) {
    data.documentTypeId = e.value;
  }

  changeName(e, file) {
    file.fullName = e.target.value;
  }

  changeSurname(e, file) {
    file.surname = e.target.value;
  }

  goBack(e) {
    this.route.navigateByUrl('documents');
  }
}
