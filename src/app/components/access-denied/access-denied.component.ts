import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth-services';

@Component({
  selector: 'app-access-denied',
  templateUrl: './access-denied.component.html',
  styleUrls: ['./access-denied.component.css']
})
export class AccessDeniedComponent implements OnInit {

  constructor(private route: Router, private auth: AuthService) { }

  ngOnInit() {
  }

  goBack(e) {
    if (this.auth.isLoggedIn()) {
      this.route.navigateByUrl('dashboard');
    } else {
      this.route.navigateByUrl('');
    }
  }
}
