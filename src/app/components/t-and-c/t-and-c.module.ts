import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TAndCComponent } from './t-and-c.component';
import { PanelModule, CardModule } from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    PanelModule,
    CardModule
  ],
  declarations: [TAndCComponent],
  exports: [TAndCComponent]
})
export class TAndCModule { }
