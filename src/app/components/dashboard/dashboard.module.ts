import { BarcodeModule } from './../doctor/barcode/barcode.module';
import { MessageModule } from './../mail/message/message.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { MenuModule, CheckboxModule, PanelModule, DropdownModule, ScheduleModule, MessagesModule } from 'primeng/primeng';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MenuModule,
    CheckboxModule,
    PanelModule,
    DropdownModule,
    ScheduleModule,
    FormsModule,
    MessageModule,
    MessagesModule,
    BarcodeModule
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }

