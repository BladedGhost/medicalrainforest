import { BarcodeComponent } from './../doctor/barcode/barcode.component';
import { style } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
// import {CarService} from '../service/carservice';
// import {EventService} from '../service/eventservice';
// import {Car} from '../domain/car';
import { SelectItem, Message } from 'primeng/primeng';
import { MenuItem } from 'primeng/primeng';
import { SessionService } from '../../services/session-service';

@Component({
    templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {

    cities: SelectItem[];


    chartData: any;

    events: any[];

    selectedCity: any;

    items: MenuItem[];
    msgs: Array<Message> = new Array<Message>();
    header: any;
    user: any;
    isdoctor: boolean;

    constructor(private store: SessionService) { }

    ngOnInit() {
        this.user = this.store.GetUserData();
        if (this.user.data.doctorId !== null) {
            if (this.user.data.hasTnC === false && this.user.data.disableTnC === false) {
                this.msgs.push({
                    severity: 'information', summary: 'Terms and Conditions',
                    detail: 'You have not yet setup your terms and conditions, \
                    you can go to your profile to upload your terms and conditions.'});
            }

            this.isdoctor = true;
        }
        this.cities = [];
        this.cities.push({ label: 'Select City', value: null });
        this.cities.push({ label: 'New York', value: { id: 1, name: 'New York', code: 'NY' } });
        this.cities.push({ label: 'Rome', value: { id: 2, name: 'Rome', code: 'RM' } });
        this.cities.push({ label: 'London', value: { id: 3, name: 'London', code: 'LDN' } });
        this.cities.push({ label: 'Istanbul', value: { id: 4, name: 'Istanbul', code: 'IST' } });
        this.cities.push({ label: 'Paris', value: { id: 5, name: 'Paris', code: 'PRS' } });

        this.chartData = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [
                {
                    label: 'First Dataset',
                    data: [65, 59, 80, 81, 56, 55, 40],
                    fill: false,
                    borderColor: '#FFC107'
                },
                {
                    label: 'Second Dataset',
                    data: [28, 48, 40, 19, 86, 27, 90],
                    fill: false,
                    borderColor: '#03A9F4'
                }
            ]
        };

        this.items = [
            { label: 'Save', icon: 'ui-icon-check' },
            { label: 'Update', icon: 'ui-icon-refresh' },
            { label: 'Delete', icon: 'ui-icon-delete' }
        ];

        this.header = {
            left: 'prev, next today',
            center: 'title',
            right: 'month, agendaWeek, agendaDay'
        };
    }
}
