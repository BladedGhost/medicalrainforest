import { Days } from './../../constants/storage-constants';
import { UploadConditionsComponent } from './upload-conditions/upload-conditions.component';
import { MessageService } from 'primeng/components/common/messageservice';
import { SessionService } from './../../services/session-service';
import { SelectItem, Messages } from 'primeng/primeng';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { PasswordValidation } from '../../helpers/PasswordValidation';
import { AuthService } from '../../services/auth-services';
import { GlobalMessagesService } from '../../services/global-messages.service';
import { Router } from '@angular/router';
import { LookupService } from '../../services/lookup-service';
import { UsernameValidator } from '../../helpers/UsernameValidator';
import { StorageConstants } from '../../constants/storage-constants';

import * as moment from 'moment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  registrationType: SelectItem[];
  discipline: SelectItem[];
  disableReg = true;
  isDoctor = '';
  doctorType = 0;
  registrationForm: FormGroup;
  user: any;
  showTnCs: boolean;
  downloadTnCUrl: string;
  options = {
    leavedays: [
    ],
    workingdays: [
    ]
  };

  constructor(private message: GlobalMessagesService, private route: Router, private lookup: LookupService,
    private auth: AuthService, private fb: FormBuilder, public usernameValidator: UsernameValidator,
    private store: SessionService, private messageService: MessageService) {
    this.registrationType = [
      { label: 'Patient', value: 0 },
      { label: 'Doctor', value: 1 }
    ];
  }

  addLeaveDay() {
    this.options.leavedays.push({
        description: '',
        startDate: null,
        endDate: null
      });
  }

  deleteLeaveDay(item) {
    const index = this.options.leavedays.indexOf(item, 0);
    if (index > -1) {
      this.options.leavedays.splice(index, 1);
    }
  }

  showResponse(response) {
    return true;
  }
  submitForm(data) {

  }
  ngOnInit() {
    this.user = this.store.GetUserData();
    const userTmp = Object.assign({}, this.user.data);
    userTmp.role = [];
    userTmp.oldPassword = '';
    if (this.user.isDoctor) {
      this.options = this.fixTime(JSON.parse(userTmp.options));
    }

    delete userTmp.menu;
    delete userTmp.dicipline;
    delete userTmp.status;
    delete userTmp.success;
    delete userTmp.updatedBy;
    delete userTmp.lockedOut;
    delete userTmp.loggedIn;
    // userTmp.options = {
    //   workingDays: null,
    //   leaveDays: null,
    //   patientBooking: false,
    //   bookingsUpdate: false,
    //   bookingsTimeframUpdate: false
    // };

    this.downloadTnCUrl = this.lookup.GetTnCDownloadUrl(this.user.data.doctorId);
    this.registrationForm = this.fb.group({
      'userId': new FormControl(0, []),
      'doctorId': new FormControl(0, []),
      'name': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2)])),
      'surname': new FormControl('', Validators.compose([Validators.required, Validators.minLength(2)])),
      'email': new FormControl('', Validators.compose([Validators.required, Validators.email])),
      'phone': new FormControl('', Validators.required),
      'username': new FormControl({value: '', disabled: true}, []),
      'oldPassword': new FormControl('', []),
      'password': new FormControl('', []),
      'confirmPassword': new FormControl('', []),
      'idPass': new FormControl(''),
      'diciplineId': new FormControl(''),
      'hpcsa': new FormControl(''),
      'practice': new FormControl(''),
      'practicePhone': new FormControl(''),
      'acceptOnlineAppointments': new FormControl(false, []),
      'role': [],
      'location': new FormControl([]),
      'uid': new FormControl([]),
      'disableTnC': new FormControl(false, []),
      'hasTnC': null,
      'registeredFromApp': new FormControl(true, []),
      'options': new FormControl('', [])
  }, {validator: PasswordValidation.MatchPassword});

    this.checkTypeSection(this.doctorType);
    this.lookup.GetLookupsFormated('Dicipline').subscribe(x => {
      this.discipline = x;
    });

    this.isDoctor = !isNaN(this.user.data.doctorId) && this.user.data.doctorId !== null ? 'Doctor' : 'Patient';
    this.registrationForm.setValue(userTmp);
    this.checkTypeSection(this.isDoctor === 'Doctor' ? 1 : 0);
  }
  onAccept(e) {
    const checked = e.checked;
    this.disableReg = !checked;
  }


  fixTime(options) {
    options.workingdays.forEach(x => {
      if (x.startTime !== '' && x.startTime !== null) {
        x.startTime = moment(x.startTime).toDate();
      }
      if (x.endTime !== '' && x.endTime !== null) {
        x.endTime = moment(x.endTime).toDate();
      }
    });
    options.leavedays.forEach(x => {
      if (x.startDate !== '' && x.startDate !== null) {
        x.startDate = moment(x.startDate).toDate();
      }
      if (x.endDate !== '' && x.endDate !== null) {
        x.endDate = moment(x.endDate).toDate();
      }
    });
    return options;
  }
  // createOptions() {
  //   return this.fb.group({
  //     workingDays: this.fb.array(this.createWorkingDays()),
  //     leaveDays: this.fb.array([]),
  //     patientBooking: new FormControl(false, []),
  //     bookingsUpdate: new FormControl(false, []),
  //     bookingsTimeframUpdate: new FormControl(false, [])
  //   });
  // }

  // createWorkingDays() {
  //   const result = new Array<FormGroup>();
  //   for (let i = 0; i < 7; i++) {
  //     result.push(this.fb.group({
  //       day: new FormControl(Days[i], []),
  //       workingHours: new FormControl('', []),
  //       enabled: new FormControl(true, [])
  //     }));
  //   }

  //   return result;
  // }

  // createLeaveDay() {

  // }

  save() {
    this.registrationForm.controls['options'].setValue(JSON.stringify(this.options));
    this.auth.Update(this.registrationForm.value).subscribe(x => {
      this.messageService.add({severity: 'success', summary: 'User Profile', detail: 'Profile have been saved successfully.'});
      const data = x.json();
      Object.keys(this.registrationForm.controls).forEach(key => {
        this.user.data[key] = this.registrationForm.controls[key].value;
      });
      this.user.data.hasTnC = data.hasTnC;
      this.user.data.options = this.registrationForm.controls['options'].value;
      this.store.SetValue(StorageConstants.UserSettings, this.user);
    });
  }

  register(e) {
    if (this.registrationForm.invalid) {
      alert('Please complete everything.');
      return false;
    }

    this.auth.Register(this.registrationForm.value).subscribe(x => {
      this.auth.Login({username: this.registrationForm.value.username, password: this.registrationForm.value.password});
    });
  }

  goBack(e) {
    this.route.navigateByUrl('dashboard');
  }

  typeChanged(e) {
    this.isDoctor = e === 0 ? 'Patient' : 'Doctor';
    this.checkTypeSection(e);
  }

  checkTypeSection(e) {
    this.registrationForm.get('role').setValue(e === 0 ? 'Patient' : 'Doctor');
    if (e === 0) {
      this.registrationForm.get('idPass').setValidators([]);
      this.registrationForm.get('diciplineId').setValidators([]);
      this.registrationForm.get('hpcsa').setValidators([]);
      this.registrationForm.get('practice').setValidators([]);
      this.registrationForm.get('practicePhone').setValidators([]);
    } else {
      this.registrationForm.get('idPass').setValidators(Validators.required);
      this.registrationForm.get('diciplineId').setValidators(Validators.required);
      this.registrationForm.get('hpcsa').setValidators(Validators.required);
      this.registrationForm.get('practice').setValidators(Validators.required);
      this.registrationForm.get('practicePhone').setValidators(Validators.required);
    }
    this.registrationForm.get('idPass').updateValueAndValidity();
    this.registrationForm.get('diciplineId').updateValueAndValidity();
    this.registrationForm.get('hpcsa').updateValueAndValidity();
    this.registrationForm.get('practice').updateValueAndValidity();
    this.registrationForm.get('practicePhone').updateValueAndValidity();
  }

  openUpload() {
    this.showTnCs = true;
  }

  onUploadCancel() {
    this.showTnCs = false;
  }

  onUploadDone(e) {
    if (e.success) {
      this.messageService.add({severity: 'success', summary: 'TnC Upload', detail: 'Terms and Conditions has been uploaded successfully'});
      this.showTnCs = false;
      this.user = e.data;
    } else {
      this.messageService.add({severity: 'error', summary: 'TnC Upload',
        detail: 'Could not upload the Terms and Conditions, please try again or contact your local administrator.'});
    }
  }
}
