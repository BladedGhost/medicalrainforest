import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadConditionsComponent } from './upload-conditions.component';

describe('UploadConditionsComponent', () => {
  let component: UploadConditionsComponent;
  let fixture: ComponentFixture<UploadConditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadConditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
