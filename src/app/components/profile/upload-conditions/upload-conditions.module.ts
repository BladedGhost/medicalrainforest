import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadConditionsComponent } from './upload-conditions.component';
import { FileUploadModule } from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    FileUploadModule
  ],
  declarations: [UploadConditionsComponent],
  exports: [UploadConditionsComponent]
})
export class UploadConditionsModule { }
