import { SessionService } from './../../../services/session-service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LookupService } from '../../../services/lookup-service';
import { StorageConstants } from '../../../constants/storage-constants';

@Component({
  selector: 'app-upload-conditions',
  templateUrl: './upload-conditions.component.html',
  styleUrls: ['./upload-conditions.component.css']
})
export class UploadConditionsComponent implements OnInit {

  @Output() cancel = new EventEmitter();
  @Output() termsUploaded = new EventEmitter();
  user: any;
  url: string;

  constructor(private store: SessionService, private _lookup: LookupService) {}

  ngOnInit() {
    this.user = this.store.GetUserData();
    this.url = this._lookup.GetTnCUploadUrl();
  }

  onCancel() {
    this.cancel.emit();
  }

  onUpload(e) {
    if (e.xhr.status === 200) {
     this.user.data.hasTnC = true;
     this.user.data.disableTnC = false;
     this.store.SetValue(StorageConstants.UserSettings, this.user);
    }
    e.success = e.xhr.status === 200;
    e.data = this.user;
    this.termsUploaded.emit(e);
  }

  onBeforeSend(e) {
    e.formData.append('userId', this.user.data.userId);
  }
}
