import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PanelModule, CardModule, InputTextModule, DropdownModule, ButtonModule,
  InputSwitchModule, InputTextareaModule, DialogModule, TabViewModule, FieldsetModule, CalendarModule } from 'primeng/primeng';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { UploadConditionsModule } from './upload-conditions/upload-conditions.module';

@NgModule({
  imports: [
    CommonModule,
    PanelModule,
    CardModule,
    InputTextModule,
    DropdownModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    InputSwitchModule,
    InputTextareaModule,
    DialogModule,
    UploadConditionsModule,
    TabViewModule,
    FieldsetModule,
    CalendarModule
  ],
  declarations: [ProfileComponent],
  exports: [ProfileComponent]
})
export class ProfileModule { }
