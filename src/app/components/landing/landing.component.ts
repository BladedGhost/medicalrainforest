import { StorageConstants } from './../../constants/storage-constants';
import { SessionService } from './../../services/session-service';
import { MessageService } from 'primeng/components/common/messageservice';
import { MailService } from './../../services/mail.service';
import { EmailSimple } from './../../models/email-simple';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from 'primeng/primeng';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  emailFormGroup: FormGroup;
  msgs: Message[] = [];
  constructor(private route: Router, private fb: FormBuilder, private email: MailService,
  private messageService: MessageService, private store: SessionService) {
    document.getElementsByClassName('layout-main')[0].classList.remove('main-margin');
  }

  ngOnInit() {
    this.emailFormGroup = this.fb.group({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      message: new FormControl('', [Validators.required])
    });
  }

  login(e) {
    this.route.navigateByUrl('signin');
  }

  send(e) {
    const model: EmailSimple = new EmailSimple();
    model.body = `<p>From ${this.emailFormGroup.value.name}</p>\
                  <p>${this.emailFormGroup.value.message}</p>`;
    model.from = this.emailFormGroup.value.email;
    model.subject = `Contact ${this.emailFormGroup.value.name}`;
    model.updatedBy = this.emailFormGroup.value.name;

    this.email.SendSupportEmail(model).subscribe(x => {
      this.emailFormGroup.setValue({
        name: '',
        email: '',
        message: ''
      });
      this.messageService.add({severity: 'information', summary: 'Contact Us', detail: 'Your message has been sent and we\
       will get back to you shortley.'});
    });
  }

  clickAboutUs(e) {
    this.route.navigateByUrl('About-Us');
  }

  clickSignIn(e) {
    this.store.SetValue(StorageConstants.SignUp, false);
    this.route.navigateByUrl('signin');

  }

  clickSignUp(e) {
    this.store.SetValue(StorageConstants.SignUp, true);
    this.route.navigateByUrl('signup');
  }

  scrollTo(control) {
    const contr = document.getElementById(control);
  }
}
